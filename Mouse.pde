//--------------------------------------- mousePressed() ----------------------------------
void mousePressed()
{  
    if (!ctrl_locked)
    {
    //------------ cube_positions of the mouse in a cube --------------
    
    //                    ┌───┬───┬───┐
    //                    │ 0 │ 1 │ 2 │
    //                    ├───┼───┼───┤
    //                    │ 3 │ 4 │ 5 │
    //                    ├───┼───┼───┤
    //                    │ 6 │ 7 │ 8 │
    //                    └───┴───┴───┘    

        find_cube();    
        switch (cube_position)
        {
            case 0:  
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
                move_one_locked  = true;
                previous_type    = cubeList[i_cube][j_cube].type;
                previous_part[0] = cubeList[i_cube][j_cube].part[0];
                previous_part[1] = cubeList[i_cube][j_cube].part[1];
                previous_part[2] = cubeList[i_cube][j_cube].part[2];
                previous_part[3] = cubeList[i_cube][j_cube].part[3];
        
                xOffset = mouseX-bx; 
                yOffset = mouseY-by;
                
                break;
            case 6:  //  rotate clockwise
                switch(cubeList[i_cube][j_cube].type)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                    case 8:
                        for (int k = 0; k<2; k++)
                        {
                            if (cubeList[i_cube][j_cube].part[k] != 0)
                            {
                                if (cubeList[i_cube][j_cube].part[k] == 8)
                                {
                                    cubeList[i_cube][j_cube].part[k] = 0;
                                }
                                cubeList[i_cube][j_cube].part[k] = byte(cubeList[i_cube][j_cube].part[k]+1);
                            }
                        }
                        break;
                    case 4:
                        if (cubeList[i_cube][j_cube].part[0] != 0)
                        {
                            if (cubeList[i_cube][j_cube].part[0] > 12)
                            {
                                cubeList[i_cube][j_cube].part[0] = -3;
                            }
                            cubeList[i_cube][j_cube].part[0] = byte(cubeList[i_cube][j_cube].part[0]+4);
                        }
                        break;
                    case 5:
                        if (cubeList[i_cube][j_cube].part[0] != 0)
                        {
                            if (cubeList[i_cube][j_cube].part[0] > 21)
                            {
                                cubeList[i_cube][j_cube].part[0] = -2;
                            }
                            cubeList[i_cube][j_cube].part[0] = byte(cubeList[i_cube][j_cube].part[0]+3);
                        }
                        break;
                }
                break;
            case 8:  //  rotate conterclockwise
                switch(cubeList[i_cube][j_cube].type)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                    case 8:
                        for (int k = 0; k<2; k++)
                        {
                            if (cubeList[i_cube][j_cube].part[k] != 0)
                            {
                                if (cubeList[i_cube][j_cube].part[k] == 1)
                                {
                                    cubeList[i_cube][j_cube].part[k] = 8;
                                }
                                cubeList[i_cube][j_cube].part[k] = byte(cubeList[i_cube][j_cube].part[k]-1);
                            }
                        }
                        break;
                    case 4:
                        if (cubeList[i_cube][j_cube].part[0] != 0)
                        {
                            if (cubeList[i_cube][j_cube].part[0] == 1)
                            {
                                cubeList[i_cube][j_cube].part[0] = 16;
                            }
                            cubeList[i_cube][j_cube].part[0] = byte(cubeList[i_cube][j_cube].part[0]-1);
                        }
                        break;
                    case 5:
                        if (cubeList[i_cube][j_cube].part[0] != 0)
                        {
                            if (cubeList[i_cube][j_cube].part[0] == 1)
                            {
                                cubeList[i_cube][j_cube].part[0] = 24;
                            }
                            cubeList[i_cube][j_cube].part[0] = byte(cubeList[i_cube][j_cube].part[0]-1);
                        }
                        break;
                }
                break;
        }
    }
    else
    {
        previous_x = mouseX;
        previous_y = mouseY;               
    }
}
//-------------------------------------- mouseDragged() -------------------------------------  
void mouseDragged()
{
    if(!ctrl_locked)
    { 
        if(!move_one_locked)
        {
            bx = mouseX-xOffset; 
            by = mouseY-yOffset;
        }
    }
    else
    {
        noFill();
        rect(previous_x, previous_y, mouseX-previous_x, mouseY-previous_y);
        
        for (int x = edge; x<width; x = x+edge)
        {
            int i = floor(x/edge)-1;
            for (int y = edge; y<height; y = y+edge)
            {
                int j = floor(y/edge)-1;
                if (((x >= previous_x) && (x <= mouseX+edge) && (y >= previous_y) && (y <= mouseY+edge))
                 || ((x <= previous_x+edge) && (x >= mouseX) && (y <= previous_y+edge) && (y >= mouseY)))
                {
                    if (SelectedCubeList[i][j].selected)
                    {
                        cubeList[i][j].selected = false;
                    }
                    else
                    {
                        cubeList[i][j].selected = true;
                        if (SelectedCubeList[i][j].ID == 0)
                        {
                            order++;
                            if ((order > 0) && (order < max_cube+1))
                            {
                                SelectedCubeList[i][j].ID = 999;
                                RailIDList[255].block_piece_x[order-1] = cubeList[i][j].x0;
                                RailIDList[255].block_piece_y[order-1] = cubeList[i][j].y0;
                                //  The first cube give the type of the element
                                if (order == 1)
                                {
                                    RailIDList[255].type = byte(cubeList[i][j].type);
                                }
                            }
                            else
                            {
                                max_cube_reached = true;
                            }
                        }
                    }
                }
            }
        }        
    }    
}
//-------------------------------------- mouseReleased() ---------------------------------------
void mouseReleased()
{
    if ((ctrl_locked) || (copy_locked) || (cut_locked))
    {
        for(int i = 1; i<max_column; i++)
        {
            for (int j = 0; j<max_line; j++)
            {                                           
                SelectedCubeList[i][j].selected = cubeList[i][j].selected;
            }
        }  
    }
    else
    {
        if ((cube_position == 0) || (cube_position == 1) || (cube_position == 2) || (cube_position == 4) || (cube_position == 5))
        {
            move_one_locked = false;
            if (i_cube != 0)
            {
                cubeList[i_cube][j_cube].type = 0;
            }
            find_cube();
            cubeList[i_cube][j_cube].type    = byte(previous_type);
            cubeList[i_cube][j_cube].part[0] = byte(previous_part[0]);
            cubeList[i_cube][j_cube].part[1] = byte(previous_part[1]);
            cubeList[i_cube][j_cube].part[2] = byte(previous_part[2]);
            cubeList[i_cube][j_cube].part[3] = byte(previous_part[3]);
        }
    }
}