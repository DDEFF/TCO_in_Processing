//*********************************** class SelectedCube ***********************************************
class SelectedCube
{ 
    int  x0;
    int  y0;
    byte type;
    byte[] part = new byte[4];
    int ID;
    boolean selected = false;
    
    SelectedCube (int aX0, int aY0, byte aType, byte aPart_00, byte aPart_01, byte aPart_02, byte aPart_03, int aID, boolean aSelected)
    {
        x0       = aX0;
        y0       = aY0;
        type     = aType;
        part[0]  = aPart_00;
        part[1]  = aPart_01;
        part[2]  = aPart_02;
        part[3]  = aPart_03;
        ID       = aID;
        selected = aSelected;
    }
}