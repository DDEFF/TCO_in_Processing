//------------------------------------- draw cube() ---------------------------------------
void draw_cube(int aX, int aY, int aI, int aJ, byte aPosition, float aRed, float aGreen, float aBlue)
{
    if (cubeList[aI][aJ].selected == false)
    {
        fill(normal_cube_red, normal_cube_green, normal_cube_blue);        //  Normal color of backgroun for the cube
    }
    else
    {
        fill(selected_cube_red, selected_cube_green, selected_cube_blue);  //  Selected color of background for the cube  
    }
    
    if (grid)
    {
        stroke(normal_cube_red, normal_cube_green, normal_cube_blue);
    }
    color corner_color = get(aX+3, aY-3);
    rect(aX, aY, edge, edge);

    if (aY != 0)
    {
        fill(corner_color);
        noStroke();
        triangle(aX, aY, aX+2.2*diag, aY, aX, aY+2.2*diag);
    }
    
    switch (cubeList[aI][aJ].type)
    {
        case 0 :
            break;
        case 1:  //  cube_line
            cubeList[aI][aJ].cube_line(aX, aY, cubeList[aI][aJ].part[0], aRed, aGreen, aBlue);
            break;
        case 2:  //  cube_curve
            cubeList[aI][aJ].cube_curve(aX, aY, cubeList[aI][aJ].part[0], aRed, aGreen, aBlue);
            break;
        case 3:  //  cube_turnout
            cubeList[aI][aJ].cube_turnout(aX, aY, cubeList[aI][aJ].part[0], cubeList[aI][aJ].part[1], boolean(aPosition), aRed, aGreen, aBlue);
            break;
        case 4:  //  cube_double_slip
            cubeList[aI][aJ].cube_double_slip(aX, aY, cubeList[aI][aJ].part[0], aRed, aGreen, aBlue);
            break;
        case 5:  //  triple
            cubeList[aI][aJ].cube_triple(aX, aY, cubeList[aI][aJ].part[0], aRed, aGreen, aBlue);
            break;
        case 6:  //  cube_crossing 90°
            cubeList[aI][aJ].cube_crossing(aX, aY, cubeList[aI][aJ].part[0], cubeList[aI][aJ].part[1]);
            break;
        case 7:  //  cube_crossing 45°
            cubeList[aI][aJ].cube_crossing(aX, aY, cubeList[aI][aJ].part[0], cubeList[aI][aJ].part[1]);
            break;
        case 8:  //  cube_buffer_stop
            cubeList[aI][aJ].cube_buffer_stop(aX, aY, cubeList[aI][aJ].part[0]);
            break;
    }
    reflection(aX, aY, edge);
}
//------------------------------------- reflection() -------------------------------------------
void reflection (int aX0, int aY0, int edge)
{    
    if (!grid)
    {
        stroke(255, 255, 255);
        line(aX0+1, aY0+1, aX0+edge-1, aY0+1);
        line(aX0+2, aY0+2, aX0+edge-2, aY0+2);
        line(aX0+1, aY0+1, aX0+1, aY0+edge-1);
        line(aX0+2, aY0+2, aX0+2, aY0+edge-2);
        
        stroke(0, 0, 0);
        line(aX0+edge, aY0, aX0+edge, aY0+edge);
        line(aX0, aY0+edge, aX0+edge, aY0+edge);
    }
}