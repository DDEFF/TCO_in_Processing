//----------------------------------- keyPressed() --------------------------------------
void keyPressed()
{
    if (key != CODED)
    {
        switch (key)
        {
            case 'S':  //------------------- Save Control Panel & RailID -------------------
            case 's':
                int count = 0;
                for(int i = 0; i<max_column; i++)
                {
                    for (int j = 0; j<max_line; j++)
                    {
                        if (cubeList[i][j].type != 0)
                        {
                            count++;
                            panel.setInt(count, "x0"      , cubeList[i][j].x0);
                            panel.setInt(count, "y0"      , cubeList[i][j].y0);
                            panel.setInt(count, "type"    , cubeList[i][j].type);
                            panel.setInt(count, "part[0]" , cubeList[i][j].part[0]);
                            panel.setInt(count, "part[1]" , cubeList[i][j].part[1]);
                            panel.setInt(count, "part[2]" , cubeList[i][j].part[2]);
                            panel.setInt(count, "part[3]" , cubeList[i][j].part[3]);
                            panel.setInt(count, "ID"      , cubeList[i][j].ID);
                        }
                    }
                }
            
                saveTable(panel,"data/Cube.tsv");
                
                count = 0;
                for(int i = 0; i<256; i++)
                {
                    if (RailIDList[i].ID != 0)
                    {
                        count++;
                        railID.setInt(count,"ID"            , RailIDList[i].ID);
                        railID.setInt(count,"type"          , RailIDList[i].type);
                        railID.setInt(count,"position"      , RailIDList[i].position);
                        railID.setInt(count,"direction"     , 0);
                        railID.setInt(count,"busy"          , 0);
                        railID.setInt(count,"signal"        , 0);
                        railID.setInt(count,"number_pieces" , RailIDList[i].number_pieces);
                        for (byte m = 0; m<max_cube; m++)
                        {
                            railID.setInt(count, "block_piece_x["+m+"]" , RailIDList[i].block_piece_x[m]);
                            railID.setInt(count, "block_piece_y["+m+"]" , RailIDList[i].block_piece_y[m]);
                        }
                    }
                }
            
                saveTable(railID,"data/RailID.tsv");
                showMessageDialog(null, "Cube table & RailID table saved", "Info", INFORMATION_MESSAGE);
                break;
            case 'L':  //---------------- Load Control Panel & RailID -----------------------
            case 'l':
                panel = loadTable("data/Cube.tsv", "header");
                
                for (TableRow row : panel.rows())
                {
                    int x0      = row.getInt("x0");
                    int y0      = row.getInt("y0");
                    int type    = row.getInt("type");
                    int part_01 = row.getInt("part[0]");
                    int part_02 = row.getInt("part[1]");
                    int part_03 = row.getInt("part[2]");
                    int part_04 = row.getInt("part[3]");
                    int ID      = row.getInt("ID");
                    
                    cubeList[x0][y0].x0      = x0;
                    cubeList[x0][y0].y0      = y0;
                    cubeList[x0][y0].type    = byte(type);
                    cubeList[x0][y0].part[0] = byte(part_01);
                    cubeList[x0][y0].part[1] = byte(part_02);
                    cubeList[x0][y0].part[2] = byte(part_03);
                    cubeList[x0][y0].part[3] = byte(part_04);
                    cubeList[x0][y0].ID      = ID;
                }            
               
                panel.clearRows();
                
                for (int i = 0; i<(max_column*max_line/5); i++)
                {
                    TableRow newRow = panel.addRow();
                    newRow.setInt("x0"      , cubeList[0][0].x0);
                    newRow.setInt("y0"      , cubeList[0][0].y0);
                    newRow.setInt("type"    , cubeList[0][0].type);
                    newRow.setInt("part[0]" , cubeList[0][0].part[0]);
                    newRow.setInt("part[1]" , cubeList[0][0].part[1]);
                    newRow.setInt("part[2]" , cubeList[0][0].part[2]);
                    newRow.setInt("part[3]" , cubeList[0][0].part[3]);
                    newRow.setInt("ID"      , cubeList[0][0].ID);
                }

                railID = loadTable("data/RailID.tsv", "header");

                for (TableRow row : railID.rows())
                {
                    int ID              = row.getInt("ID");
                    int type            = row.getInt("type");
                    int position        = row.getInt("position");
                    int direction       = row.getInt("direction");
                    int busy            = row.getInt("busy");
                    int signal          = row.getInt("signal");                   
                    int number_pieces   = row.getInt("number_pieces");
                    for (int i = 0; i < max_cube-1; i++)
                    {
                        block_piece_x[i] = row.getInt("block_piece_x["+i+"]");
                        block_piece_y[i] = row.getInt("block_piece_y["+i+"]");
                    }

                    RailIDList[ID].ID                = ID;
                    RailIDList[ID].type              = byte(type);
                    RailIDList[ID].position          = byte(position);
                    RailIDList[ID].direction         = boolean(direction);
                    RailIDList[ID].busy              = boolean(busy);
                    RailIDList[ID].signal            = byte(signal);
                    RailIDList[ID].number_pieces     = byte(number_pieces);
                    for (int i = 0; i < max_cube-1; i++)
                    {
                        RailIDList[ID].block_piece_x[i]  = block_piece_x[i];
                        RailIDList[ID].block_piece_y[i]  = block_piece_y[i];
                    }
                }
                
                railID.clearRows();
                
                for (int i = 0; i<256; i++)
                {
                    TableRow newRow = railID.addRow();
                    newRow.setInt("ID"            , RailIDList[i].ID);
                    newRow.setInt("type"          , RailIDList[i].type);
                    newRow.setInt("position"      , RailIDList[i].position);
                    newRow.setInt("direction"     , 0); 
                    newRow.setInt("busy"          , 0);
                    newRow.setInt("signal"        , 0);       
                    newRow.setInt("number_pieces" , RailIDList[i].number_pieces);
                    for (byte m = 0; m<max_cube; m++)
                    {
                        newRow.setInt("block_piece_x["+m+"]" , RailIDList[i].block_piece_x[m]);
                        newRow.setInt("block_piece_y["+m+"]" , RailIDList[i].block_piece_y[m]);
                    }
                }
                break;
            case 'C':  //----------------- copy (after CTRL) --------------------
                shift_C_locked = true;
            case 'c':
            case 'X':  //----------------- cut  (after CTRL) --------------------
            case 'x':
                if (ctrl_locked)
                {
                    clear_selectedCube();
                    count = 0;
                    for(int i = 1; i<max_column; i++)
                    {
                        for (int j = 0; j<max_line; j++)
                        {
                            //  Construction of the foreplan
                            //  It's not usefull to retain all the selectionned cubes
                            //  only those which have rails
                            
                            if ((cubeList[i][j].selected) && (cubeList[i][j].type != 0))
                            {
                                count++;
                                if (count == 1)
                                {
                                    //  reference cube
                                    x_ref = cubeList[i][j].x0;
                                    y_ref = cubeList[i][j].y0;
                                }
                                else
                                {
                                    //  All the cubes are referenced to the reference cube (x_ref, y_ref)
                                    SelectedCubeList[i][j].x0 = cubeList[i][j].x0 - x_ref;
                                    SelectedCubeList[i][j].y0 = cubeList[i][j].y0 - y_ref;
                                }
                                SelectedCubeList[i][j].type     = cubeList[i][j].type;
                                SelectedCubeList[i][j].part[0]  = cubeList[i][j].part[0];
                                SelectedCubeList[i][j].part[1]  = cubeList[i][j].part[1];
                                SelectedCubeList[i][j].part[2]  = cubeList[i][j].part[2];
                                SelectedCubeList[i][j].part[3]  = cubeList[i][j].part[3];
                                SelectedCubeList[i][j].ID       = cubeList[i][j].ID;
                                SelectedCubeList[i][j].selected = true;
                            }
                        }
                    }        
                    
                    ctrl_locked = false;
                    cursor(ARROW);
                    
                    if ((key == 'C') || (key == 'c'))
                    {
                        copy_locked = true;
                    }
                    else
                    {
                        cut_locked = true;
                    }
                    break;
                }
            case 'V':  //----------------- Paste (after copy or cut) --------------------
            case 'v':
                if ((copy_locked) || (cut_locked))
                {
                    count = 0;
                    
                    for(int i = 1; i<max_column; i++)
                    {
                        for (int j = 0; j<max_line; j++)
                        {
                            if (SelectedCubeList[i][j].selected)
                            {
                                count++;
                                if (count == 1)
                                {
                                    i_cube = floor(mouseX/edge);
                                    j_cube = floor(mouseY/edge);  
                                    
                                    cubeList[i_cube][j_cube].type     = SelectedCubeList[i][j].type;
                                    cubeList[i_cube][j_cube].part[0]  = SelectedCubeList[i][j].part[0];
                                    cubeList[i_cube][j_cube].part[1]  = SelectedCubeList[i][j].part[1];
                                    cubeList[i_cube][j_cube].part[2]  = SelectedCubeList[i][j].part[2];
                                    cubeList[i_cube][j_cube].part[3]  = SelectedCubeList[i][j].part[3];
                                    cubeList[i_cube][j_cube].ID       = SelectedCubeList[i][j].ID;
                                    cubeList[i_cube][j_cube].selected = false;
                                    
                                    offset_x = i_cube-i;
                                    offset_y = j_cube-j;
                                }
                                else
                                {
                                    i_cube = floor(mouseX+edge*SelectedCubeList[i][j].x0)/edge;
                                    j_cube = floor(mouseY+edge*SelectedCubeList[i][j].y0)/edge;
                                    
                                    cubeList[i_cube][j_cube].type     = SelectedCubeList[i][j].type;
                                    cubeList[i_cube][j_cube].part[0]  = SelectedCubeList[i][j].part[0];
                                    cubeList[i_cube][j_cube].part[1]  = SelectedCubeList[i][j].part[1];
                                    cubeList[i_cube][j_cube].part[2]  = SelectedCubeList[i][j].part[2];
                                    cubeList[i_cube][j_cube].part[3]  = SelectedCubeList[i][j].part[3];
                                    cubeList[i_cube][j_cube].ID       = SelectedCubeList[i][j].ID;                                    
                                    cubeList[i_cube][j_cube].selected = false;
                                }
                            }
                        }
                    }
                    
                    if (copy_locked)
                    {      
                        if (!shift_C_locked)
                        {
                            copy_locked = false;
                            //  All the selected cubes must be cleared
                            clear_selectedCube();
                            clear_selected_in_cubeList();
                        }
                    }
                    else  //  = cut_locked
                    {                
                        listID_search();                      //  search all different ID in selected cubes
                        railID_offset(offset_x, offset_y);    //  offset of labels of selected cubes
                        cut_locked= false;
                        
                        //  All the marked cubes must be cleared
                        clear_cubeList();
                        //  All the selected cubes must be cleared
                        clear_selectedCube();
                        clear_selected_in_cubeList();
                    }                
                    
                }
                break;
            case 'M':  //------------------ Memory ------------------------
            case 'm':
                if (ctrl_locked)
                {
                    memory_locked = !memory_locked;
                }

                if (!max_cube_reached)
                {
                    String id = showInputDialog("Please enter new ID");
                   
                    if (id == null)
                    {
                        copy_locked    = false;
                        shift_C_locked = false;
                        cut_locked     = false;
                        ctrl_locked    = false;

                        //  All the selected cubes must be cleared
                        clear_selectedCube();
                        clear_selected_in_cubeList();
                    }
                    else
                    {
                        if ("".equals(id))
                        {
                            showMessageDialog(null, "Empty ID input", "Alert", ERROR_MESSAGE);
                        }               
                        else
                        {
                            if (int(id) > 0)
                            {
                                for (int i = 0; i<max_column; i++)
                                {
                                    for (int j = 0; j<max_line; j++)
                                    {
                                        if (SelectedCubeList[i][j].ID == 999)
                                        {
                                            RailIDList[int(id)].ID            = int(id);                                            
                                            RailIDList[int(id)].number_pieces = order;
                                            RailIDList[int(id)].type          = RailIDList[255].type;
                                            for (byte order = 0; order<max_cube; order++)
                                            {
                                                RailIDList[int(id)].block_piece_x[order] = RailIDList[255].block_piece_x[order];
                                                RailIDList[int(id)].block_piece_y[order] = RailIDList[255].block_piece_y[order];
                                            }
                                            cubeList[i][j].ID = int(id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    showMessageDialog(null, "More than "+max_cube+" cubes selected", "Info", INFORMATION_MESSAGE);
                    max_cube_reached = false;
                }
                clear_selected_in_cubeList();
                clear_selectedCube();
                clear_railID(255);
                ctrl_locked = false;
                cursor(ARROW);
                break;
            case 'd':
                block_text = !block_text;
                break;
            case 'E':
            case 'e':
                exemple = !exemple;
                break;
            case 'G':
            case 'g':
                grid = !grid;
                break;
            case 'D':
                block_signal = !block_signal;
                break;
        }
    }
    else
    {
        switch (keyCode)
        {
            case CONTROL:  //----------------- Control (for copy, cut or memory) ------------------------
            {
                if (!ctrl_locked)
                {
                    ctrl_locked = true;
                    cursor(MOVE);
                    order = 0;
                }
                break;
            }
        }
    }
}

//---------------- clear_cubeList() -------------------------
void clear_cubeList()
{
    for(int i = 1; i<max_column; i++)
    {
        for (int j = 0; j<max_line; j++)
        {                            
            if (cubeList[i][j].selected)
            {                
                cubeList[i][j].type     = 0;
                cubeList[i][j].part[0]  = 0;
                cubeList[i][j].part[1]  = 0;
                cubeList[i][j].part[2]  = 0;
                cubeList[i][j].part[3]  = 0;
                cubeList[i][j].ID       = 0;
                cubeList[i][j].selected = false;
            }
        }
    }  
}

//--------------------clear_selectedCube() --------------------------
void clear_selectedCube()
{
    for(int i = 1; i<max_column; i++)
    {
        for (int j = 0; j<max_line; j++)
        {                            
            SelectedCubeList[i][j].x0       = 0;
            SelectedCubeList[i][j].y0       = 0;                                                
            SelectedCubeList[i][j].type     = 0;
            SelectedCubeList[i][j].part[0]  = 0;
            SelectedCubeList[i][j].part[1]  = 0;
            SelectedCubeList[i][j].part[2]  = 0;
            SelectedCubeList[i][j].part[3]  = 0;
            SelectedCubeList[i][j].ID       = 0;
            SelectedCubeList[i][j].selected = false;
        }
    }
}
//------------------- clear_selected_in_cubeList() -------------------
void clear_selected_in_cubeList()
{
    for(int i = 1; i<max_column; i++)
    {
        for (int j = 0; j<max_line; j++)
        {                            
            if (cubeList[i][j].selected)
            {                
                cubeList[i][j].selected = false;
            }
        }
    }    
}
//------------------- clear_railID() -------------------
void clear_railID(int aLine)
{
    RailIDList[aLine].ID            = aLine;                                            
    RailIDList[aLine].type          = 0;
    RailIDList[aLine].position      = 0;
    RailIDList[aLine].direction     = false;
    RailIDList[aLine].busy          = false;
    RailIDList[aLine].signal        = 0;
    RailIDList[aLine].number_pieces = 0;
    for (byte order = 0; order<max_cube; order++)
    {
        RailIDList[255].block_piece_x[order] = 0;
        RailIDList[255].block_piece_y[order] = 0;
    }
}
//------------------ RailID offset() ----------------------------
void railID_offset(int aOffset_x, int aOffset_y)
{       
    for (int i = 0; i<255; i++)
    {
        for (int k = 0; k<count_ID+1; k++)
        {
            if (RailIDList[i].ID == list_ID[k])
            {
                for (int j = 0; j<max_cube; j++)
                {
                    if (RailIDList[i].block_piece_x[j] != 0)
                    {
                        RailIDList[i].block_piece_x[j] = RailIDList[i].block_piece_x[j]+aOffset_x;
                        RailIDList[i].block_piece_y[j] = RailIDList[i].block_piece_y[j]+aOffset_y;
                    }
                }
            }
        }
    }
}
//---------------- listID_search() ------------------------------------
void listID_search()
{
    count_ID = 0;
    for(int i = 1; i<max_column; i++)
    {
        for (int j = 0; j<max_line; j++)
        {                            
            for (int k = 0; k<count_ID+1; k++)
            {
                find_new_ID = false;
                if (SelectedCubeList[i][j].ID == list_ID[k])
                {                
                    find_new_ID = true;
                    break;
                }
            }
            if (!find_new_ID)
            {
                count_ID = count_ID+1;
                list_ID[count_ID] = SelectedCubeList[i][j].ID;
            }
        }
    }    
}