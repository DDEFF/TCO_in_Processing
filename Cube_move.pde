void cube_move()
{
    //----------------------------- Mouve only one cube --------------------------------------------------
    if (move_one_locked)
    {        
        if (mouseX < edge+1)
        {
            mouseX = edge+1; 
        }

        x_ref = floor(mouseX-edge/4);
        y_ref = floor(mouseY-edge/4);
        draw_cube(x_ref, y_ref, i_cube, j_cube, byte(0), rail_forecolor_red, rail_forecolor_green, rail_forecolor_blue);        
    }
    //------------------------ Copy or cut one one or many cube(s) --------------------------------------------
    if ((copy_locked) || (cut_locked))
    {
        if (mouseX < edge+1)
        {
            mouseX = edge+1; 
        }
 
        int k = 0;
        
        for (int i = 1; i<max_column; i++)
        {
            for (int j = 0; j<max_line; j++)
            {
                if (SelectedCubeList[i][j].type != 0)
                {
                    k++;
                    if (k == 1)
                    {
                        x_ref  = floor(mouseX-edge/4);
                        y_ref  = floor(mouseY-edge/4);
                        draw_cube(x_ref, y_ref, i, j, byte(0), rail_forecolor_red, rail_forecolor_green, rail_forecolor_blue);
                    }
                    else
                    {
                        fill(normal_cube_red, normal_cube_green, normal_cube_blue);    //  Normal cube's color
                        
                        x_ref = mouseX-edge/4+edge*SelectedCubeList[i][j].x0;
                        y_ref = mouseY-edge/4+edge*SelectedCubeList[i][j].y0;
                        draw_cube(x_ref, y_ref, i, j, byte(0), rail_forecolor_red, rail_forecolor_green, rail_forecolor_blue);
                    }
                }
            }
        }
    }
}
//-------------------------------------- find_cube() -----------------------------------
void find_cube()
{
    i_cube = floor(mouseX/edge);
    j_cube = floor(mouseY/edge);
    x_cube = i_cube*edge;
    y_cube = j_cube*edge;
    
    if ((mouseX > x_cube) && (mouseX < x_cube+edge/3))
    {
        if ((mouseY > y_cube) && (mouseY < y_cube+edge/3))
        {
            cube_position = 0;
        }
        else
        {
            if ((mouseY > y_cube+edge/3) && (mouseY < y_cube+2*edge/3))
            {
                cube_position = 3;
            }
            else
            {
                if ((mouseY > y_cube+2*edge/3) && (mouseY < y_cube+edge))
                {
                    cube_position = 6;
                }
            }
        }
    }
    else
    {
        if ((mouseX > x_cube+edge/3) && (mouseX < x_cube+2*edge/3))
        {
            if ((mouseY > y_cube) && (mouseY < y_cube+edge/3))
            {
                cube_position = 1;
            }
            else
            {
                if ((mouseY > y_cube+edge/3) && (mouseY < y_cube+2*edge/3))
                {
                    cube_position = 4;
                }
                else
                {
                    if ((mouseY > y_cube+2*edge/3) && (mouseY < y_cube+edge))
                    {
                        cube_position = 7;
                    }
                }
            }
        }
        else
        {
            if ((mouseX > x_cube+2*edge/3) && (mouseX < x_cube+edge))
            {
                if ((mouseY > y_cube) && (mouseY < y_cube+edge/3))
                {
                    cube_position = 2;
                }
                else
                {
                    if ((mouseY > y_cube+edge/3) && (mouseY < y_cube+2*edge/3))
                    {
                        cube_position = 5;
                    }
                    else
                    {
                        if ((mouseY > y_cube+2*edge/3) && (mouseY < y_cube+edge))
                        {
                            cube_position = 8;
                        }
                    }
                }
            }
        }
    }
    if (mouseX < edge)
    {
        cube_position = 4;
    }
}