//*********************************** class Cube ***********************************************
class Cube
{ 
    int     x0;
    int     y0;
    byte    type;
    byte[]  part     = new byte[4];
    boolean selected = false;
    float   red;
    float   green;
    float   blue;
    boolean position1 = false;
    boolean position2 = false;
    int     ID;
    
    Cube (int aX0, int aY0, byte aType, byte aPart_00, byte aPart_01, byte aPart_02, byte aPart_03, boolean aSelected,
          float aRed, float aGreen, float aBlue, boolean aPosition1, boolean aPosition2, int aID)
    {
        x0        = aX0;
        y0        = aY0;
        type      = aType;
        part[0]   = aPart_00;
        part[1]   = aPart_01;
        part[2]   = aPart_02;
        part[3]   = aPart_03;
        selected  = aSelected;
        red       = aRed;
        green     = aGreen;
        blue      = aBlue;
        position1 = aPosition1;
        position2 = aPosition2;
        ID        = aID;
    }
    
    //-------------------cube's orientation ---------
    
    //                    2      3     4
    //                      ┌────────┐
    //                      │        │
    //                    1 │        │ 5
    //                      │        │
    //                      └────────┘
    //                    8     7      6
    
    //--------------------------------- cube_line ----------------------------------------------
    void cube_line (int aX0, int aY0, byte aPart, float aRed, float aGreen, float aBlue)
    {
        fill(aRed, aGreen, aBlue);
        stroke(aRed, aGreen, aBlue);
        switch (aPart)
        {
            case 1:
            case 5:
                rect(aX0-thickness, aY0+edge/2-thickness, edge+thickness, 2*thickness);
                break;
            case 2:
            case 6:
                quad(aX0+edge+diag, aY0+edge-diag, aX0+edge-diag, aY0+edge+diag, aX0-2*diag, aY0, aX0, aY0-2*diag);
                break;
            case 3:
            case 7:
                rect(aX0+edge/2-thickness, aY0, 2*thickness, edge);
                break;
            case 4:
            case 8:
                quad(aX0+edge-diag, aY0-diag, aX0+edge+diag, aY0+diag, aX0, aY0+edge+2*diag, aX0-2*diag, aY0+edge); 
                break;
        }
    }
    //--------------------------------- cube_curve ----------------------------------------------
    void cube_curve (int aX0, int aY0, byte aPart, float aRed, float aGreen, float aBlue)
    {
        noFill();
        strokeWeight(2.4*thickness);
        stroke(aRed, aGreen, aBlue);
        switch (aPart)
        {
            case 1:  //  2-5
                bezier(aX0+edge, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0-diag, aY0-diag);
                break;
            case 2:  //  3-6
                bezier(aX0+edge/2, aY0-diag, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge, aY0+edge);
                break;
            case 3:  //  4-7
                bezier(aX0+edge/2, aY0+edge, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge+diag, aY0-diag);
                break;
            case 4:  //  5-8
                bezier(aX0+edge, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0-diag, aY0+edge+diag);
                break;
            case 5:  //  1-6
                bezier(aX0-diag, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge, aY0+edge);
                break;
            case 6:  //  2-7
                bezier(aX0+edge/2, aY0+edge, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0-diag, aY0-diag);
                break;
            case 7:  //  3-8
                bezier(aX0+edge/2, aY0-diag, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0-diag, aY0+edge+diag); 
                break;
            case 8:  //  1-4
                bezier(aX0-diag, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge/2, aY0+edge/2, aX0+edge+diag, aY0-diag);
                break;
        }
        strokeWeight(1);
    }
    //-------------------------- cube_turnout ------------------------------------------------------
    void cube_turnout (int aX0, int aY0, byte aPart1, byte aPart2, boolean aPosition1, float aRed, float aGreen, float aBlue)
    {
        if (aPosition1)
        {
            stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
            cube_line (aX0, aY0, aPart2, rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
            stroke(draw_red, draw_green, draw_blue);
            cube_curve (aX0, aY0, aPart1, aRed, aGreen, aBlue);
        }
        else
        {
            stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
            cube_curve (aX0, aY0, aPart1, rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
            stroke(draw_red, draw_green, draw_blue);
            cube_line (aX0, aY0, aPart2, aRed, aGreen, aBlue);
        }
    }
    //-------------------------- cube_double_slip ------------------------------------------------------
    void cube_double_slip (int aX0, int aY0, byte aPart, float aRed, float aGreen, float aBlue)
    {
        switch (aPart)
        {
            case 1:  //  1-5               
                double_slip_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  1-5
                break;
            case 2:  //  1-6
                double_slip_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(5), aRed, aGreen, aBlue);  //  1-6
                break;
            case 3:  //  2-5
                double_slip_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  2-5
                break;
            case 4:  //  2-6                
                double_slip_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  2-6
                break;
            case 5:  //  2-6
                double_slip_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  2-6
                break;
            case 6:  //  2-7
                double_slip_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(6), aRed, aGreen, aBlue);  //  2-7
                break;
            case 7:  //  3-6
                double_slip_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  3-6
                break;
            case 8:  //  3-7
                double_slip_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  3-7
                break;
            case 9:  //  3-7
                double_slip_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  3-7
                break;
            case 10:  //  3-8
                double_slip_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(7), aRed, aGreen, aBlue);  //  3-8
                break;
            case 11:  //  4-7
                double_slip_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  4-7
                break;
            case 12:  //  4-8
                double_slip_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  4-8
                break;
            case 13:  //  4-8
                double_slip_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  4-8
                break;
            case 14:  //  1-4
                double_slip_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(8), aRed, aGreen, aBlue);  //  1-4
                break;
            case 15:  //  5-8
                double_slip_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  5-8
                break;
            case 16:  //  1-5
                double_slip_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  1-5
                break;
        }      
    }
    //-------------------------- double_slip_1() ---------------------------------------------------
    void double_slip_1(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-5
        cube_line  (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-6
        cube_curve (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-5
        cube_curve (aX0, aY0, byte(5), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-6     
    }
    //------------------------- double_slip_2() ----------------------------------------------------
    void double_slip_2(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-7
        cube_line  (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-6
        cube_curve (aX0, aY0, byte(6), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-7
        cube_curve (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-6
    }
    //------------------------- double_slip_3() ----------------------------------------------------
    void double_slip_3(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  4-8
        cube_line  (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-7
        cube_curve (aX0, aY0, byte(7), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-8
        cube_curve (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  4-7
    }
    //------------------------- double_slip_4() ----------------------------------------------------
    void double_slip_4(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-5
        cube_line  (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  4-8
        cube_curve (aX0, aY0, byte(8), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-4
        cube_curve (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  5-8
    }
    //-------------------------- cube_triple() ------------------------------------------------------
    void cube_triple (int aX0, int aY0, byte aPart, float aRed, float aGreen, float aBlue)
    {
        switch (aPart)
        {
            case 1:  //  1-5               
                triple_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  1-5
                break;
            case 2:  //  1-4
                triple_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(8), aRed, aGreen, aBlue);  //  1-4
                break;
            case 3:  //  1-6
                triple_1(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(5), aRed, aGreen, aBlue);  //  1-6
                break;               
                
            case 4:  //  2-6                
                triple_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  2-6
                break;
            case 5:  //  2-5
                triple_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  2-5
                break;
            case 6:  //  2-7
                triple_2(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(6), aRed, aGreen, aBlue);  //  2-7
                break;                
                
            case 7:  //  3-7
                triple_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  3-7
                break;
            case 8:  //  3-6
                triple_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  3-6
                break;
            case 9:  //  3-8
                triple_3(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(7), aRed, aGreen, aBlue);  //  3-8
                break;                
                
            case 10:  //  4-8
                triple_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  4-8
                break;
            case 11:  //  4-7
                triple_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  4-7
                break;
            case 12:  //  4-1
                triple_4(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(8), aRed, aGreen, aBlue);  //  4-1
                break;                
                
            case 13:  //  5-1
                triple_5(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  5-1
                break;
            case 14:  //  5-8
                triple_5(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  5-8
                break;
            case 15:  //  5-2
                triple_5(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(1), aRed, aGreen, aBlue);  //  5-2
                break;                
                
            case 16:  //  6-2
                triple_6(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  6-2
                break;
            case 17:  //  6-1
                triple_6(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(5), aRed, aGreen, aBlue);  //  6-1
                break;
            case 18:  //  6-3
                triple_6(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(2), aRed, aGreen, aBlue);  //  6-3
                break;
      
            case 19:  //  7-3
                triple_7(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  7-3
                break;
            case 20:  //  7-2
                triple_7(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(6), aRed, aGreen, aBlue);  //  7-2
                break;
            case 21:  //  7-4
                triple_7(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(3), aRed, aGreen, aBlue);  //  7-4
                break;                
                
            case 22:  //  8-4
                triple_8(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_line  (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  8-4
                break;
            case 23:  //  8-3
                triple_8(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(7), aRed, aGreen, aBlue);  //  8-3
                break;
            case 24:  //  8-5
                triple_8(aX0, aY0);
                stroke(aRed, aGreen, aBlue);
                cube_curve (aX0, aY0, byte(4), aRed, aGreen, aBlue);  //  8-5
                break;
        }      
    }
    //------------------------- triple_1() ----------------------------------------------------
    void triple_1(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-5
        cube_curve (aX0, aY0, byte(8), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-4
        cube_curve (aX0, aY0, byte(5), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  1-6
    }
    //------------------------- triple_2() ----------------------------------------------------
    void triple_2(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-6
        cube_curve (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-5
        cube_curve (aX0, aY0, byte(6), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  2-7
    }
    //------------------------- triple_3() ----------------------------------------------------
    void triple_3(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-7
        cube_curve (aX0, aY0, byte(7), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-8
        cube_curve (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  3-6
    }
    //------------------------- triple_4() ----------------------------------------------------
    void triple_4(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  4-8
        cube_curve (aX0, aY0, byte(8), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  4-1
        cube_curve (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  4-7
    }
    //------------------------- triple_5() ----------------------------------------------------
    void triple_5(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  5-1
        cube_curve (aX0, aY0, byte(1), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  5-2
        cube_curve (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  5-8
    }
    //------------------------- triple_6() ----------------------------------------------------
    void triple_6(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  6-2
        cube_curve (aX0, aY0, byte(5), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  6-1
        cube_curve (aX0, aY0, byte(2), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  6-3
    }
    //------------------------- triple_7() ----------------------------------------------------
    void triple_7(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  7-3
        cube_curve (aX0, aY0, byte(6), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  7-2
        cube_curve (aX0, aY0, byte(3), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  7-4
    }
    //------------------------- triple_8() ----------------------------------------------------
    void triple_8(int aX0, int aY0)
    {
        stroke(rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);
        cube_line  (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  8-4
        cube_curve (aX0, aY0, byte(7), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  8-3
        cube_curve (aX0, aY0, byte(4), rail_backcolor_red, rail_backcolor_green, rail_backcolor_blue);  //  8-5
    }
    //-------------------------- cube_crossing ------------------------------------------------------
    void cube_crossing (int aX0, int aY0, byte aPart1, byte aPart2)
    {
        stroke(draw_red, draw_green, draw_blue);
        cube_line (aX0, aY0, aPart1, draw_red, draw_green, draw_blue);
        cube_line (aX0, aY0, aPart2, draw_red, draw_green, draw_blue);
    }
    //--------------------------------- cube_buffer_stop ----------------------------------------------
    void cube_buffer_stop (int aX0, int aY0, int aPart)
    {
        fill(rail_forecolor_red, rail_forecolor_green, rail_forecolor_blue);
        stroke(rail_forecolor_red, rail_forecolor_green, rail_forecolor_blue);
        switch (aPart)
        {
            case 1:
                rect(aX0, aY0+edge/2-thickness, edge/2, 2*thickness);
                rect(aX0+edge/2, aY0+edge/2-3*thickness, 2*thickness, 6*thickness);
                break;
            case 2:
                quad(aX0+edge/2+diag, aY0+edge/2-diag, aX0+edge/2-diag, aY0+edge/2+diag, aX0-diag, aY0+diag, aX0+diag, aY0-diag);
                quad(aX0+edge/2+3*diag, aY0+edge/2-3*diag, aX0+edge/2+5*diag, aY0+edge/2-diag, aX0+edge/2-diag, aY0+edge/2+5*diag, aX0+edge/2-3*diag, aY0+edge/2+3*diag);
                break;
            case 3:
                rect(aX0+edge/2-thickness, aY0, 2*thickness, edge/2);
                rect(aX0+edge/2-3*thickness, aY0+edge/2-thickness, 6*thickness, 2*thickness);
                break;
            case 4:
                quad(aX0+edge-diag, aY0-diag, aX0+edge+diag, aY0+diag, aX0+edge/2+diag, aY0+edge/2+diag, aX0+edge/2-diag, aY0+edge/2-diag);
                quad(aX0+edge/2+3*diag, aY0+edge/2+3*diag, aX0+edge/2+diag, aY0+edge/2+5*diag, aX0+edge/2-5*diag, aY0+edge/2-diag, aX0+edge/2-3*diag, aY0+edge/2-3*diag);
                break;
            case 5:
                rect(aX0+edge/2, aY0+edge/2-thickness, edge/2, 2*thickness);
                rect(aX0+edge/2-2*thickness, aY0+edge/2-3*thickness, 2*thickness, 6*thickness);
                break;
            case 6:
                quad(aX0+edge+diag, aY0+edge-diag, aX0+edge-diag, aY0+edge+diag, aX0+edge/2-diag, aY0+edge/2+diag, aX0+edge/2+diag, aY0+edge/2-diag);
                quad(aX0+edge/2-3*diag, aY0+edge/2+3*diag, aX0+edge/2-5*diag, aY0+edge/2+diag, aX0+edge/2+diag, aY0+edge/2-5*diag, aX0+edge/2+3*diag, aY0+edge/2-3*diag);
                break;
            case 7:
                rect(aX0+edge/2-thickness, aY0+edge/2, 2*thickness, edge);
                rect(aX0+edge/2-3*thickness, aY0+edge/2-2*thickness, 6*thickness, 2*thickness);
                break;
            case 8:
                quad(aX0+edge/2-diag, aY0+edge/2-diag, aX0+edge/2+diag, aY0+edge/2+diag, aX0+diag, aY0+edge+diag, aX0-diag, aY0+edge-diag);
                quad(aX0+edge/2+5*diag, aY0+edge/2+diag, aX0+edge/2+3*diag, aY0+edge/2+3*diag, aX0+edge/2-3*diag, aY0+edge/2-3*diag, aX0+edge/2-diag, aY0+edge/2-5*diag);
                break;
        }
    }
}