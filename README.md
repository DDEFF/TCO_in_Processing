Ce projet est en cours de développement.

Il consiste en la création d'un TCO (Tableau de Contrôle Optique) qui représentra votre réseau sur un écran qui peut être de grande taille.
Il est prévu que cet écran soit géré par un PCDuino.
Pour l'instant, il est directement commandé par un ordinateur (testé sous Windows, pas encore sous Mac).

La partie développée est essentiellement un éditeur graphique qui permet de dessiner un réseau de façon simple.
Est aussi développée une partie gestion du port série pour amener des informations depuis l'extérieur.

Deux programmes sont complémentaires :
1°) Un programme en qui gère le TCO sur l'écran et répond au clavier et à la souris.
2°) Un sketch Arduino qui fournit les infos au TCO.
