//*********************************** class RailID ***********************************************
class RailID
{ 
    int     ID;
    byte    type;
    byte    position;
    boolean direction      = false;
    boolean busy           = false;
    byte    signal         = 0;
    byte    number_pieces;
    int[]   block_piece_x  = new int[max_cube];
    int[]   block_piece_y  = new int[max_cube];

    public RailID (int aID, byte aType, byte aPosition, boolean aDirection, boolean aBusy, byte aSignal, byte aNumber_pieces, int aBlock_piece_x[], int aBlock_piece_y[])
    {
        ID               = aID;
        type             = aType;
        position         = aPosition;
        direction        = aDirection;
        busy             = aBusy;
        signal           = aSignal;
        number_pieces    = aNumber_pieces;

        for (int i = 0; i < max_cube-1; i++)
        {
            block_piece_x[i] = aBlock_piece_x[i];
            block_piece_y[i] = aBlock_piece_y[i];
        }
    }
};