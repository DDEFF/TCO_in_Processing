#include "Train.h"
extern Train trainList[];

#include "Variables.h"
#include <Arduino.h>

#include "Route.h"
extern Route routeList[];

#include "Block.h"
extern Block blockList[];

Train::Train(byte aCn, boolean aBusy_cn, byte aCn_plus_1, boolean aBusy_cn_plus_1, byte aCn_plus_2, boolean aBusy_cn_plus_2, boolean aThird_block_flag,
byte aTrain_signal, boolean aTravel_direction, byte aStop_block, boolean aStop_direction)
{
    this->cn                       = aCn;
    this->busy_cn                  = aBusy_cn;
    this->cn_plus_1                = aCn_plus_1;
    this->busy_cn_plus_1           = aBusy_cn_plus_1;
    this->cn_plus_2                = aCn_plus_2;
    this->busy_cn_plus_2           = aBusy_cn_plus_2;
    this->third_block_flag         = aThird_block_flag;
    this->train_signal             = aTrain_signal;
    this->travel_direction         = aTravel_direction;
    this->stop_block               = aStop_block;
    this->stop_direction           = aStop_direction;
}

//*************************** Other functions *********************************

//------------------------- PrintBlock() --------------------------
void Train::PrintTrain()
{
    Serial.println();  
    Serial.print  ("      cn,busy_cn,  cn+1,busy_cn+1,  cn+2,busy_cn+2,third,  train_signal,  direction,    Stop");
    Serial.println();
    Serial.print  ("\t");
    Serial.print  (this->cn);
    Serial.print  ("\t");
    Serial.print  (this->busy_cn);
    Serial.print  ("\t");
    Serial.print  (this->cn_plus_1);
    Serial.print  ("\t");
    Serial.print  (this->busy_cn_plus_1);
    Serial.print  ("\t");
    Serial.print  (this->cn_plus_2);
    Serial.print  ("\t");
    Serial.print  (this->busy_cn_plus_2);
    Serial.print  ("\t");
    Serial.print  (this->third_block_flag);
    Serial.print  ("\t");
    Serial.print  (this->train_signal);
    Serial.print  ("\t");
    Serial.print  ("     ");
    Serial.print  (this->travel_direction);
    Serial.print  ("\t");
    Serial.print  ("        ");
    Serial.print  (this->stop_block);
    Serial.print  ("\t");
    Serial.print  (this->stop_direction);
}

//------------------------------ ResetTrain() -------------------------------------

void Train::ResetTrain()
{
    this->cn                       = 0;
    this->busy_cn                  = FREE;
    this->cn_plus_1                = 0;
    this->busy_cn_plus_1           = FREE;
    this->cn_plus_2                = 0;
    this->busy_cn_plus_2           = FREE;
    this->third_block_flag         = false;
    this->train_signal             = 0;
    this->travel_direction         = CLOCKWISE;
}

//-------------------------- FindingIsolatedTrains() ---------------------------------------

void Train::FindingIsolatedTrains()
{
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
        if (blockList[i].getBusy() == BUSY)
        {
            //  There is a train in this block
            if ((blockList[blockList[i].getPreviousBlock()-101].getBusy() == FREE) && (blockList[blockList[i].getNextBlock()-101].getBusy() == FREE))
            {
                //  The train is alone. But it's possible that it already exists in trainList[]
                found_train = false;
                for (byte j = 0; j < MAX_TRAIN; j++)
                {
                    if (blockList[i].getCurrentBlock() == trainList[j].getCn())
                    {
                        found_train = true;
                        break;
                    }
                }
                if (found_train == false)
                {
                    //  This train doesn't exist : we must create a line for it
                    total_train = total_train + 1;
                    trainList[total_train].setCn(blockList[i].getCurrentBlock());
                    trainList[total_train].setBusyCn(BUSY);                      
                }
            }
        }
    }
}

//----------------------------- UpdateTrainList() ---------------------------

void Train::UpdateTrainList()
{
    for (byte i = 0; i < MAX_TRAIN; i++)
    {
        if (trainList[i].getCn() !=0)
        {
            //  We follow only existing trains ...
            block_line = trainList[i].getCn()-101;  //  It's the # line of the train in blockList[]
      
            if (blockList[block_line].getTrainDirection() == CLOCKWISE)      
            {
                //  The train goes clockwise
                if (blockList[blockList[block_line].getNextBlock()-101].getBusy() == BUSY)
                {
                    //  The train is on the block Cn and Cn+1 : we have to forget Cn and shift the blocks  
                    trainList[i].setCn(blockList[block_line].getNextBlock());
                    trainList[i].setBusyCn(BUSY);
                    trainList[i].setCnPlus1(blockList[blockList[block_line].getNextBlock()-101].getNextBlock());
                    trainList[i].setBusyCnPlus1(blockList[blockList[blockList[block_line].getNextBlock()-101].getNextBlock()-101].getBusy());
                    trainList[i].setTrainSignal(blockList[trainList[i].getCn()-101].getClockSignal());
                    block_line = trainList[i].getCnPlus1()-101;
                    if ((blockList[block_line].getNextBlock() == 0) || (trainList[i].getCnPlus1() == 0))
                    {
                        trainList[i].setCnPlus2(0);
                        trainList[i].setBusyCnPlus2(FREE);                      
                    }
                    else
                    {
                        trainList[i].setCnPlus2(blockList[block_line].getNextBlock());
                        trainList[i].setBusyCnPlus2(blockList[blockList[block_line].getNextBlock()-101].getBusy());          
                    }
                }
                else
                {
                    //  The train is only on Cn
                    trainList[i].setCnPlus1(0);
                    trainList[i].setBusyCnPlus1(FREE);          
                    trainList[i].setCnPlus2(0);
                    trainList[i].setBusyCnPlus2(FREE);                      
                    trainList[i].setTrainSignal(blockList[block_line].getClockSignal());
                    trainList[i].setBusyCn(BUSY);                           
                }
                trainList[i].setTravelDirection(CLOCKWISE);
            }
            else
            {
                // The trains goes conterclockwise
                if (blockList[blockList[block_line].getPreviousBlock()-101].getBusy() == BUSY)
                {
                    //  The train is on the block Cn and Cn+1 : we must forget Cn and shift the blocks  
                    trainList[i].setCn(blockList[block_line].getPreviousBlock());
                    trainList[i].setBusyCn(BUSY);
                    trainList[i].setCnPlus1(blockList[blockList[block_line].getPreviousBlock()-101].getPreviousBlock());
                    trainList[i].setBusyCnPlus1(blockList[blockList[blockList[block_line].getPreviousBlock()-101].getPreviousBlock()-101].getBusy());
                    trainList[i].setTrainSignal(blockList[trainList[i].getCn()-101].getClockSignal());
                    block_line = trainList[i].getCnPlus1()-101;
                    if ((blockList[block_line].getNextBlock() == 0) || (trainList[i].getCnPlus1() == 0))
                    {
                        trainList[i].setCnPlus2(0);
                        trainList[i].setBusyCnPlus2(FREE);                      
                    }
                    else
                    {
                        trainList[i].setCnPlus2(blockList[block_line].getPreviousBlock());
                        trainList[i].setBusyCnPlus2(blockList[blockList[block_line].getPreviousBlock()-101].getBusy());
                    }          
                }
                else
                {
                    //  The train is only on Cn
                    trainList[i].setCnPlus1(0);
                    trainList[i].setBusyCnPlus1(FREE);          
                    trainList[i].setCnPlus2(0);
                    trainList[i].setBusyCnPlus2(FREE);                      
                    trainList[i].setTrainSignal(blockList[block_line].getClockSignal());
                    trainList[i].setBusyCn(BUSY);                           
                }        
                trainList[i].setTravelDirection(CONTERCLOCKWISE);
            }
        }
    }  
}

//--------------------------- OppositeTrains() ---------------------------------------

void Train::OppositeTrains()
{
    for (byte i = 0; i < MAX_TRAIN; i++)
    {
        if (trainList[i].getStopBlock() == 0)
        {
            //  it's the first time there is an opposition for this trains
            for (byte j = i+1; j < MAX_TRAIN; j++)
            {
                if ((trainList[i].getCnPlus2() == trainList[j].getCnPlus2()) && (trainList[i].getCnPlus2() != 0))
                {
          
                    if (trainList[j].getTravelDirection() == CLOCKWISE)
                    {
                        blockList[trainList[j].getCnPlus1()-101].setClockSignal(S);        //  Set signal to semaphore
                        trainList[j].setStopBlock(trainList[j].getCnPlus1());
                        trainList[j].setStopDirection(CLOCKWISE);
                        blockList[trainList[j].getCn()-101].setClockSignal(10*floor(blockList[trainList[j].getCn()-101].getClockSignal()/10)+A);  //  Set signal to avertissement
                        trainList[j].setTrainSignal(blockList[trainList[j].getCn()-101].getClockSignal());
                        blockList[trainList[i].getCnPlus2()-101].setConterSignal(S);  //  Set signal to semaphore
                        trainList[i].setStopBlock(trainList[i].getCnPlus2());
                        trainList[i].setStopDirection(CONTERCLOCKWISE);
                        blockList[trainList[i].getCnPlus1()-101].setConterSignal(10*floor(blockList[trainList[i].getCnPlus1()-101].getConterSignal()/10)+A);  //  Set signal to avertissement
                    }
                    else
                    {
                        blockList[trainList[j].getCnPlus1()-101].setConterSignal(S);  //  Set signal to semaphore
                        trainList[j].setStopBlock(trainList[j].getCnPlus1());
                        trainList[j].setStopDirection(CONTERCLOCKWISE);
                        blockList[trainList[j].getCn()-101].setConterSignal(10*floor(blockList[trainList[j].getCn()-101].getConterSignal()/10)+A);  //  Set signal to avertissement
                        trainList[j].setTrainSignal(blockList[trainList[j].getCn()-101].getClockSignal());
                        blockList[trainList[i].getCnPlus2()-101].setClockSignal(S);        //  Set signal to semaphore
                        trainList[i].setStopBlock(trainList[i].getCnPlus2());
                        trainList[i].setStopDirection(CLOCKWISE);
                        blockList[trainList[i].getCnPlus1()-101].setClockSignal(10*floor(blockList[trainList[i].getCnPlus1()-101].getClockSignal()/10)+A);    //  Set signal to avertissement
                    }      
                }
            }
        }
        else
        {
            //  the trains are already in opposition
            if (trainList[i].getTravelDirection() != trainList[i].getStopDirection())
            {
                //  The trains aren't any more in opposition
                if (trainList[i].getStopDirection() == CLOCKWISE)
                {
                    //  The train goes clockwise
                    for (byte j = 0; j < MAX_TRAIN ; j++)
                    {
                        if (trainList[j].getStopBlock() == blockList[trainList[i].getStopBlock()-101].getNextBlock())
                        {
                            //  the opposite trains are connected by contigous blocks 
                            trainList[j].setStopBlock(0);
                            trainList[j].setStopDirection(CLOCKWISE);
                            break;
                        }
                    }
                    trainList[i].setStopBlock(0);
                    trainList[i].setStopDirection(CLOCKWISE);          
                }
                else
                {
                    //  the train goes conterclockwise
                    for (byte j = 0; j < MAX_TRAIN; j++)
                    {
                        if (trainList[j].getStopBlock() == blockList[trainList[i].getStopBlock()-101].getPreviousBlock())
                        {
                            //  the opposite trains are connected by contigous blocks 
                            trainList[j].setStopBlock(0);
                            trainList[j].setStopDirection(CLOCKWISE);
                            break;
                        }
                    }
                    trainList[i].setStopBlock(0);
                    trainList[i].setStopDirection(CLOCKWISE);                                
                }
            }
            else
            {
                //  the trains are always in opposition
                if (trainList[i].getStopDirection() == CLOCKWISE)
                {
                    blockList[trainList[i].getStopBlock()-101].setClockSignal(S);        //  Set signal to semaphore
                    blockList[blockList[trainList[i].getStopBlock()-101].getPreviousBlock()-101].setClockSignal(10*floor(blockList[blockList[trainList[i].getStopBlock()-101].getPreviousBlock()-101].getClockSignal()/10)+A);  //  Set signal to avertissement
                    trainList[i].setTrainSignal(blockList[trainList[i].getCn()-101].getClockSignal());
                    for (byte j = 0; j < MAX_TRAIN; j++)
                    {
                        if (trainList[j].getStopBlock() == blockList[trainList[i].getStopBlock()-101].getNextBlock())
                        {
                            blockList[trainList[j].getStopBlock()-101].setConterSignal(S);        //  Set signal to semaphore
                            blockList[blockList[trainList[j].getStopBlock()-101].getNextBlock()-101].setConterSignal(10*floor(blockList[blockList[trainList[j].getStopBlock()-101].getNextBlock()-101].getConterSignal()/10)+A);  //  Set signal to avertissement
                            trainList[j].setTrainSignal(blockList[trainList[j].getCn()-101].getConterSignal());
                            break;
                        }
                    }
                }
                else
                {
                  blockList[trainList[i].getStopBlock()-101].setConterSignal(S);        //  Set signal to semaphore
                  blockList[blockList[trainList[i].getStopBlock()-101].getNextBlock()-101].setConterSignal(10*floor(blockList[blockList[trainList[i].getStopBlock()-101].getNextBlock()-101].getConterSignal()/10)+A);  //  Set signal to avertissement
                  trainList[i].setTrainSignal(blockList[trainList[i].getCn()-101].getConterSignal());
                  for (byte j = 0; j < MAX_TRAIN; j++)
                  {
                      if (trainList[j].getStopBlock() == blockList[trainList[i].getStopBlock()-101].getPreviousBlock())
                      {
                          blockList[trainList[j].getStopBlock()-101].setClockSignal(S);        //  Set signal to semaphore
                          blockList[blockList[trainList[j].getStopBlock()-101].getPreviousBlock()-101].setClockSignal(10*floor(blockList[blockList[trainList[j].getStopBlock()-101].getPreviousBlock()-101].getClockSignal()/10)+A);  //  Set signal to avertissement
                          trainList[j].setTrainSignal(blockList[trainList[j].getCn()-101].getClockSignal());
                          break;
                      }
                  }          
                }
            }
        }
    }  
}

//--------------------------- ThreeBlocks() -----------------------------------------------

void Train::ThreeBlocks()
{
    for (byte i = 0; i < MAX_TRAIN; i++)
    {
        if ((trainList[i].getBusyCn() == BUSY) && (trainList[i].getBusyCnPlus1() == BUSY) && (trainList[i].getBusyCnPlus2() == BUSY))
        {
            //  The train occupies three blocks
            trainList[i].setThirdBlockFlag(true);
            trainList[i].setTrainSignal(S);
            if (trainList[i].getTravelDirection() == CLOCKWISE)
            {
                blockList[trainList[i].getCnPlus2()-101].setClockSignal(S);
            }
            else
            {
                blockList[trainList[i].getCnPlus2()-101].setConterSignal(S);
            } 
        }
    }  
}


