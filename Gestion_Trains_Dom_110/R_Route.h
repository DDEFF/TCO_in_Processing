#ifndef R_ROUTE_H 
#define R_ROUTE_H
#include <Arduino.h>

#include "Define.h"

class R_Route    // R_Route for "Reserved Route"
{
    public :
        // ---------------------- Default constructor ---------------------------
        R_Route()
        {
            ResetR_Route();
        };
        
        // --------------------- Overload constructors -----------------------
        R_Route(byte r_start_block, byte r_turnout[MAX_TURNOUT_ROUTE], boolean r_turnout_path[MAX_TURNOUT_ROUTE], byte r_end_block , byte r_priority);
        
        // ******************** Accessor functions ***************************
        
        inline byte    getR_StartBlock()              const { return this->r_start_block; }
        inline byte    getR_Turnout    (byte aIndex1) const { return this->r_turnout     [aIndex1]; }
        inline boolean getR_TurnoutPath(byte aIndex2) const { return this->r_turnout_path[aIndex2]; }
        inline byte    getR_EndBlock()                const { return this->r_end_block; }
        inline byte    getR_Priority()                const { return this->r_priority; }
        inline byte    getR_First()                   const { return this->r_first; }
                
        // ****************** Mutator functions *******************************
        
        inline void setR_StartBlock  (byte aR_Start_block)             { this->r_start_block           = aR_Start_block; }
        inline void setR_Turnout     (byte aIndex1, byte    aValue1)   { this->r_turnout     [aIndex1] = aValue1; }
        inline void setR_TurnoutPath (byte aIndex2, boolean aValue2)   { this->r_turnout_path[aIndex2] = aValue2; }
        inline void setR_EndBlock    (byte aR_End_block)               { this->r_end_block             = aR_End_block; }
        inline void setR_Priority    (byte aR_Priority)                { this->r_priority              = aR_Priority; }
        inline void setR_First       (byte aR_First)                   { this->r_first                 = aR_First; }
        
        //********************** Other functions **************************************     
    
        void PrintR_Route();
            //  print reserved routes
          
        void ResetR_Route();
            //  reset all values, except of priorities
          
        void R_RouteExchange();
            //  exchange between W_Route and R_Route
          
        void PutTurnoutPosition();
            //  put turnouts in position
          
        static void RouteResetTesting();
            //  test if the w_route and the r_route could be cleared
          
        static void RouteChoice();
            //  calculating the origine_block and the extremity_block. Afterwards, we can fill the w_route. 
          
    private :
    
        // Members variables
        
        // Depending of turnouts positions :
        
          byte    r_start_block;                     // # of start block (opposite wanted train direction)
          byte    r_turnout     [MAX_TURNOUT_ROUTE]; // Here : 20 turnouts from 0 to 19
          boolean r_turnout_path[MAX_TURNOUT_ROUTE]; // path of each turnout : STRAIGHT (false) or DIVERGING (true)      
          byte    r_end_block;                       // # of end block (opposite wanted train direction)
          byte    r_priority;                        // ranking of the routes by priority
          byte    r_first;                           // first turnout of r_route
        
};

#endif

