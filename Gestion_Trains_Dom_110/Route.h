#ifndef ROUTE_H 
#define ROUTE_H
#include <Arduino.h>

#include "Define.h"

class Route    // Route is the real route
{
    public :
        // ---------------------- Default constructor ---------------------------
        Route()
        {
            ResetRoute();
        };
        
        // --------------------- Overload constructors -----------------------    
    
        Route(byte start_block, byte turnout[MAX_TURNOUT_ROUTE], byte end_block, byte max_route_speed);
        
        // ******************** Accessor functions ***************************
        
        inline byte getStartBlock()         const { return this->start_block; }
        inline byte getTurnout(byte aIndex) const { return this->turnout[aIndex]; }
        inline byte getEndBlock()           const { return this->end_block; }
        inline byte getRouteMaxSpeed()      const { return this->max_route_speed; }
                
        // ****************** Mutator functions *******************************
        
        inline void setStartBlock    (byte aStart_block)        { this->start_block     = aStart_block; }
        inline void setTurnout       (byte aIndex, byte aValue) { this->turnout[aIndex] = aValue; }
        inline void setEndBlock      (byte aEnd_block)          { this->end_block       = aEnd_block; }
        inline void setRouteMaxSpeed (byte aMax_route_speed)    { this->max_route_speed = aMax_route_speed; }
        
        //********************** Other functions **************************************  
        void PrintRoute();
            // print simultaneous routes
        
        void ResetRoute();
            // reset all values
          
        void NewRouteInit(byte aBlock, byte aTurnout);
            //  new route tests the real position of turnouts. First : initialization.
          
        void AddTurnout(byte aTurn01,byte aTurn02);
            // add a turnout to the initialized route
          
        void TurnoutFilling();
            // add turnouts
            
    private :
    
        // Members variables
        
        // Depending of turnouts positions :
        
          byte start_block;                  // # of start block (clockwise)
          byte turnout[MAX_TURNOUT_ROUTE];   // Here : 20 turnouts from 0 to 19
          byte end_block;                    // # of end block (clockwise)
          byte max_route_speed;              // max speed on the route : 255, 160, 60, 30 (km/h)
          
};

#endif

