#ifndef BLOCK_H
#define BLOCK_H
#include <Arduino.h>

#include "Define.h"

class Block
{
    public :
        // ---------------------- Default constructor ---------------------------
        Block()
       {
           this->previous_element           = 0;
           this->current_block              = 0;
           this->next_element               = 0;
           this->block_level                = 0;
           this->block_length               = 0;
           this->stop_length                = 0;
           this->conter_stop_length         = 0;
           this->previous_block             = 0;
           this->next_block                 = 0;
           this->clock_signal               = 0;
           this->conter_signal              = 0;
           this->busy                       = FREE;
           this->train_direction            = CLOCKWISE;
           this->clw_stop_zone_status       = FREE;
           this->plain_track_status         = FREE;
           this->cclw_stop_zone_status      = FREE;
           this->clw_stop_zone_status_0     = FREE;
           this->plain_track_status_0       = FREE;
           this->cclw_stop_zone_status_0    = FREE;
       };
        
        // --------------------- Overload constructors -----------------------
        Block(
        byte previous_element,
        byte current_block,
        byte next_element,
        byte block_level,
        byte block_length                 = 0,
        byte stop_length                  = 0,
        byte conter_stop_length           = 0,
        byte previous_block               = 0,
        byte next_block                   = 0,
        byte clock_signal                 = 0,
        byte conter_signal                = 0,
        boolean busy                      = FREE,
        boolean train_direction           = CLOCKWISE,
        boolean clw_stop_zone_status      = FREE,
        boolean plain_track_status        = FREE,
        boolean cclw_stop_zone_status     = FREE,
        boolean clw_stop_zone_status_0    = FREE,
        boolean plain_track_status_0      = FREE,
        boolean cclw_stop_zone_status_0   = FREE);
        
        // ******************** Accessor functions ***************************
        
        inline byte    getPreviousElement()                const { return this->previous_element; }        
        inline byte    getCurrentBlock()                   const { return this->current_block; }      
        inline byte    getNextElement()                    const { return this->next_element; }
        inline byte    getBlockLevel()                     const { return this->block_level; } 
        inline byte    getBlockLength()                    const { return this->block_length; }      
        inline byte    getStopLength()                     const { return this->stop_length; }      
        inline byte    getConterStopLength()               const { return this->conter_stop_length; }
        inline byte    getPreviousBlock()                  const { return this->previous_block; }      
        inline byte    getNextBlock()                      const { return this->next_block; }     
        inline byte    getClockSignal()                    const { return this->clock_signal; }      
        inline byte    getConterSignal()                   const { return this->conter_signal; }   
        inline boolean getBusy()                           const { return this->busy; }      
        inline boolean getTrainDirection()                 const { return this->train_direction; }
        inline boolean getClwStopZoneStatus()              const { return this->clw_stop_zone_status; }
        inline boolean getPlainTrackStatus()               const { return this->plain_track_status; }
        inline boolean getCclwStopZoneStatus()             const { return this->cclw_stop_zone_status; }
        inline boolean getClwStopZoneStatus0()             const { return this->clw_stop_zone_status_0; }
        inline boolean getPlainTrackStatus0()              const { return this->plain_track_status_0; }
        inline boolean getCclwStopZoneStatus0()            const { return this->cclw_stop_zone_status_0; }
        
          
        // ****************** Mutator functions *******************************
    
        inline void setPreviousElement                (byte    aPrevious_element)                   { this->previous_element                   = aPrevious_element; }     
        inline void setCurrentBlock                   (byte    aCurrent_block)                      { this->current_block                      = aCurrent_block; }      
        inline void setNextElement                    (byte    aNext_element)                       { this->next_element                       = aNext_element; }      
        inline void setBlockLevel                     (byte    aBlock_level)                        { this->block_level                        = aBlock_level; }
        inline void setBlockLength                    (byte    aBlock_length)                       { this->block_length                       = aBlock_length; }     
        inline void setStopLength                     (byte    aStop_length)                        { this->stop_length                        = aStop_length; }      
        inline void setConterStopLength               (byte    aConter_stop_length)                 { this->conter_stop_length                 = aConter_stop_length; }
        inline void setPreviousBlock                  (byte    aPrevious_block)                     { this->previous_block                     = aPrevious_block; }
        inline void setNextBlock                      (byte    aNext_block)                         { this->next_block                         = aNext_block; }       
        inline void setClockSignal                    (byte    aClock_signal)                       { this->clock_signal                       = aClock_signal; }      
        inline void setConterSignal                   (byte    aConter_signal)                      { this->conter_signal                      = aConter_signal; }
        inline void setBusy                           (boolean aBusy)                               { this->busy                               = aBusy; }
        inline void setTrainDirection                 (boolean aTrain_direction)                    { this->train_direction                    = aTrain_direction; }
        inline void setClwStopZoneStatus              (boolean aClw_stop_zone_status)               { this->clw_stop_zone_status               = aClw_stop_zone_status; }
        inline void setPlainTrackStatus               (boolean aPlain_track_status)                 { this->plain_track_status                 = aPlain_track_status; }
        inline void setCclwStopZoneStatus             (boolean aCclw_stop_zone_status)              { this->cclw_stop_zone_status              = aCclw_stop_zone_status; }
        inline void setClwStopZoneStatus0             (boolean aClw_stop_zone_status_0)             { this->clw_stop_zone_status_0             = aClw_stop_zone_status_0; }
        inline void setPlainTrackStatus0              (boolean aPlain_track_status_0)               { this->plain_track_status_0               = aPlain_track_status_0; }
        inline void setCclwStopZoneStatus0            (boolean aCclw_stop_zone_status_0)            { this->cclw_stop_zone_status_0            = aCclw_stop_zone_status_0; }
    
        //******************************** Other functions ***********************************************
    
        void PrintBlock();
            // print fixed values in block
          
        void NextBlock();
            // next block
          
        void NextBlockRoute(byte aCurrent_element, boolean aDirection);
            // next block after a turnout
        
        static void SignalsFilling();
            // filling all the signals of blocList[]
        
        //  This part of the sketch must be moved to a bolck-module  
  
            static void OccupancyZonesTest(boolean aClw_stop_zone_status, boolean aPlain_track_status, boolean aCclw_stop_zone_status);
                //  Test the occupancy of the parts of one block (next)
             
            static void OccupancyZonesMemo(boolean aClw_stop_zone_status, boolean aPlain_track_status, boolean aCclw_stop_zone_status);
                //  Test the occupancy of the parts of one block (previous)
             
            static void Advance();
                //  Test the memo of occupancy of the parts of the block
              
            static void DeterminingTrainsDirections();
                //  Determining the directions of the trains
                      
    private :
    
        // Members variables
        
        // Defined once and for all :
        
            byte previous_element;                     // # of previous element (clockwise)
            byte current_block;                        // # of current block
            byte next_element;                         // # of next element (clockwise)
            byte block_level;                          // level of the block in the station map
            byte block_length;                         // block length (in cm)
            byte stop_length;                          // stop length (clockwise, in cm)
            byte conter_stop_length;                   // stop length (conterclockwise, in cm)
        
        // Depending of trains and turnouts positions :
        
            byte    previous_block;                    //  # of previous block (clockwise)
            byte    next_block;                        //  # of next block (clockwise)
            byte    clock_signal;                      //  color of the signal (clockwise)
            byte    conter_signal;                     //  color of the signal (conterclockwise)
            boolean busy;                              //  busy (true) or not (false)
            boolean train_direction;                   //  train direction (clockwise (false) or conterclockwise (true))
            boolean clw_stop_zone_status;              //  busy (true) or not (false)
            boolean plain_track_status;                //  busy (true) or not (false)
            boolean cclw_stop_zone_status;             //  busy (true) or not (false)
            boolean clw_stop_zone_status_0;            //  busy (true) or not (false)
            boolean plain_track_status_0;              //  busy (true) or not (false)
            boolean cclw_stop_zone_status_0;           //  busy (true) or not (false)
 
};

#endif

