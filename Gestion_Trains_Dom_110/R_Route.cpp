#include "R_Route.h"
extern R_Route r_routeList[];

#include "Variables.h"
#include <Arduino.h>

#include "Block.h"
extern Block blockList[];

#include "Turnout.h"
extern Turnout turnoutList[];

#include "W_Route.h"
extern W_Route w_routeList[];

//-------------------------------------
R_Route::R_Route(byte aR_Start_block, byte aR_Turnout[MAX_TURNOUT_ROUTE], boolean aR_Turnout_path[MAX_TURNOUT_ROUTE], byte aR_End_block , byte aR_Priority)
{
    this->r_start_block          = aR_Start_block;  
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        this->r_turnout[i]         = aR_Turnout[i];
        this->r_turnout_path[i]    = aR_Turnout_path[i];
    }  
    this->r_end_block            = aR_End_block;
    this->r_priority             = aR_Priority;
}

//**************************** Other functions ******************************************

//------------------------------ ResetR_Route -------------------------------------
void R_Route::ResetR_Route()
{
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        if (this->r_turnout[i] != 0)
        {
            turnoutList[this->r_turnout[i]-1].setTurnoutStatus(FREE);
        }
    }
    
    this->r_start_block             = 0;  
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        this->r_turnout[i]      = 0;
        this->r_turnout_path[i] = STRAIGHT;
    }
    this->r_end_block               = 0;
}

//------------------------- PrintR_Route --------------------------

void R_Route::PrintR_Route()
{
  //  if (this->start_block > 0)
  //  {
      Serial.print(this->r_start_block);
      Serial.print(" ");    
      for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
      {
          if (this->r_turnout[i] <10)
            Serial.print(" ");
          Serial.print(this->r_turnout[i]);
          Serial.print("->");
          Serial.print(this->r_turnout_path[i]);
          Serial.print(" ");
      }
      Serial.print(this->r_end_block);
      Serial.print(" ");
      Serial.print(this->r_priority);
      Serial.print(" ");
      Serial.print(this->r_first);
  //  }
}

//--------------------------------- R_RouteExchange() ------------------------------------------------------------
//  Exchange between w_route and r_route. In particular, the order of the elements are inversed.

void R_Route::R_RouteExchange()
{
    this->r_start_block       = w_routeList[w_route_line].getW_EndBlock();
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        this->r_turnout[i]      = w_routeList[w_route_line].getW_Turnout(MAX_TURNOUT_ROUTE-1-i);
        this->r_turnout_path[i] = w_routeList[w_route_line].getW_TurnoutPath(MAX_TURNOUT_ROUTE-1-i);
    }
    this->r_end_block         = w_routeList[w_route_line].getW_StartBlock();
}

//--------------------------------- PutTurnoutPosition() ------------------------------------------------------------
//  This is for testing.

void R_Route::PutTurnoutPosition()
{
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
      if (this->r_turnout[i] != 0)
      {
          turnoutList[this->r_turnout[i]-1].setTurnoutPath(this->r_turnout_path[i]);
      }
    }
}

//------------------------------ RouteResetTesting() -------------------------------------
//  Test if the w_route and the r_route could be cleared

void R_Route::RouteResetTesting()
{          
    block_line = origine_block-101;
    for (byte i = 0; i < MAX_R_ROUTE; i++)
    {
        if ((origine_block == r_routeList[i].getR_StartBlock()) || (origine_block == r_routeList[i].getR_EndBlock()))
        {
            r_route_line = i;
            
            route_direction = CONTERCLOCKWISE;
            for (byte  j = 0; j < MAX_ROUTE; j = j+2)
            {
                if (origine_block == new_route_init[j])
                {
                    route_direction = CLOCKWISE;
                }
            }
            if (route_direction == CLOCKWISE)
            {
                current_signal = blockList[block_line].getClockSignal();
                if ((blockList[block_line].getBusy() == FREE) || (current_signal == S) || (current_signal == C) || (blockList[block_line].getTrainDirection() == CONTERCLOCKWISE))
                {
                    r_routeList[r_route_line].ResetR_Route(); 
                }
                else
                {          
                    Serial.println();
                    Serial.println("Too late to clear the route : ");
                    Serial.print(origine_block);
                    Serial.print(" - ");
                    Serial.print(extremity_block);
                    Serial.print("                      blockList[block_line].getBusy() = ");
                    Serial.print(blockList[block_line].getBusy());
                    Serial.print(" - block_line = ");
                    Serial.print(block_line);
                    Serial.print(" - current_signal = ");
                    Serial.print(current_signal);
                    Serial.print(" - blockList[block_line].getTrainDirection() = ");
                    Serial.println(blockList[block_line].getTrainDirection());

                }        
            }
            else
            {
                current_signal = blockList[block_line].getConterSignal();
                if ((blockList[block_line].getBusy() == FREE) || (current_signal == S) || (current_signal == C) || (blockList[block_line].getTrainDirection() == CLOCKWISE))
                {
                    r_routeList[r_route_line].ResetR_Route();        
                }
                else
                {
                    Serial.println();
                    Serial.println("Too late to clear the route : ");
                    Serial.print(origine_block);
                    Serial.print(" - ");
                    Serial.print(extremity_block);
                }
            }
        }
    }  
    w_routeList[w_route_line].ResetW_Route();
}

//-------------------------- Choice of a route --------------------------------
//  Calculating the origine_block and the extremity_block. Afterwards, we can fill the w_route.

void R_Route::RouteChoice()
{
    total_pin_route = 0;
    for (byte i = 0; i < MAX_W_ROUTE; i++)
    {
        if (pin_route[i] == true)
        {
            total_pin_route = total_pin_route + 1;
        }
    }

    switch (total_pin_route)
    {
        case 0 :
            origine_block = 0;
            extremity_block = 0;
            break;
        case 1 :
            for (byte i = 0; i < MAX_W_ROUTE; i++)
            {
                if (pin_route[i] == true)
                {
                    origine_block = w_routeList[i].getW_Line();
                    first_turnout = w_routeList[i].getW_First();
                    w_route_line = i;
                    RouteResetTesting();
                    break;
                }
            }
            break;
        case 2 :
            for (byte i = 0; i < MAX_W_ROUTE; i++)
            {
                if ((pin_route[i] == true) && (i != w_route_line))
                {
                    extremity_block = w_routeList[i].getW_Line();
                    w_routeList[w_route_line].W_RouteFilling();
                    last_route_date = millis();
                    break;  
                }
            }
            break;
        default :
            origine_block = 0;
            extremity_block = 0;
    }
}

