#ifndef TRAIN_H
#define TRAIN_H
#include <Arduino.h>

#include "Define.h"

class Train
{
    public :
        // ---------------------- Default constructor ---------------------------
        Train()
       {
           this->cn                       = 0;
           this->busy_cn                  = FREE;
           this->cn_plus_1                = 0;
           this->busy_cn_plus_1           = FREE;
           this->cn_plus_2                = 0;
           this->busy_cn_plus_2           = FREE;
           this->third_block_flag         = false;
           this->train_signal             = 0;
           this->travel_direction         = CLOCKWISE;
           this->stop_block               = 0;
           this->stop_direction           = CLOCKWISE;
       };
        
        // --------------------- Overload constructors -----------------------
        Train(byte cn, boolean busy_cn, byte cn_plus_1, boolean busy_cn_plus_1, byte cn_plus_2, boolean busy_cn_plus_2, boolean third_block_flag,
        byte train_signal, boolean travel_direction, byte stop_block, boolean stop_direction);
        
        // ******************** Accessor functions ***************************
        
        inline byte    getCn()                  const { return this->cn; }
        inline boolean getBusyCn()              const { return this->busy_cn; }
        inline byte    getCnPlus1()             const { return this->cn_plus_1; }      
        inline boolean getBusyCnPlus1()         const { return this->busy_cn_plus_1; }
        inline byte    getCnPlus2()             const { return this->cn_plus_2; }      
        inline boolean getBusyCnPlus2()         const { return this->busy_cn_plus_2; }
        inline boolean getThirdBlockFlag()      const { return this->third_block_flag; }
        inline byte    getTrainSignal()         const { return this->train_signal; } 
        inline boolean getTravelDirection()     const { return this->travel_direction; }
        inline byte    getStopBlock()           const { return this->stop_block; } 
        inline boolean getStopDirection()       const { return this->stop_direction; }
          
        // ****************** Mutator functions *******************************
    
        inline void setCn                  (byte       aCn)                    { this->cn                    = aCn; }      
        inline void setBusyCn              (boolean    aBusy_cn)               { this->busy_cn               = aBusy_cn; }      
        inline void setCnPlus1             (byte       aCn_plus_1)             { this->cn_plus_1             = aCn_plus_1; }     
        inline void setBusyCnPlus1         (boolean    aBusy_cn_plus_1)        { this->busy_cn_plus_1        = aBusy_cn_plus_1; }      
        inline void setCnPlus2             (byte       aCn_plus_2)             { this->cn_plus_2             = aCn_plus_2; }      
        inline void setBusyCnPlus2         (boolean    aBusy_cn_plus_2)        { this->busy_cn_plus_2        = aBusy_cn_plus_2; }      
        inline void setThirdBlockFlag      (boolean    aThird_block_flag)      { this->third_block_flag      = aThird_block_flag; }
        inline void setTrainSignal         (byte       aTrain_signal)          { this->train_signal          = aTrain_signal; }
        inline void setTravelDirection     (boolean    aTravel_direction)      { this->travel_direction      = aTravel_direction; }
        inline void setStopBlock           (byte       aStop_block)            { this->stop_block            = aStop_block; }
        inline void setStopDirection       (boolean    aStop_direction)        { this->stop_direction        = aStop_direction; }
    
        //******************************** Other functions ***********************************************
    
        void PrintTrain();
            //  print trains positions
          
        void ResetTrain();
            //  reset trains positions
        
        static void FindingIsolatedTrains();
            //  finding the trains which are on an alone block
        
        static void UpdateTrainList();
            //  Update trainList[]
          
        static void OppositeTrains();
            //  For the problem of opposite trains
        
        static void ThreeBlocks();
            //  Problem of the three blocks
         
    private :
    
        // Members variables
        
          byte    cn;                    //  # of current block position of the train
          boolean busy_cn;               //  busy cn
          byte    cn_plus_1;             //  # of next block position of the train
          boolean busy_cn_plus_1;        //  busy cn_plus_1
          byte    cn_plus_2;             //  # of next next block position of the train
          boolean busy_cn_plus_2;        //  busy_cn_plus_2
          boolean third_block_flag;      //  The train is on three blocks
          byte    train_signal;          //  Signal code as the train sees
          boolean travel_direction;      //  Direction of the train
          byte    stop_block;            //  Store the # block wich must be stopped by opposite trains 
          boolean stop_direction;        //  Store the direction of the train wich must be stopped by opposite trains
        
};

#endif

