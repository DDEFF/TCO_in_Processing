#define MAX_BLOCK         31
#define MAX_TURNOUT       21
#define MAX_ROUTE         17  // MAX_ROUTE is the maximum number of possible SIMULTANEOUS routes. Here : 9+2+2
#define MAX_R_ROUTE       12  // MAX_R_ROUTE is the maximum number of reserved routes. Here : 9+2+2
#define MAX_W_ROUTE       35  // MAX_W_ROUTE is the maximum number of wanted routes. Here : 21+4+4
#define MAX_TURNOUT_ROUTE 5
#define MAX_TRAIN         5

#define STRAIGHT          false
#define DIVERGING         true

#define FREE              0
#define BUSY              1
#define RESERVED          2
#define CONFLICTING       3

#define CLOCKWISE         false
#define CONTERCLOCKWISE   true

//                                                                                                                                           | code pgm |         |------|  |  allumage feu  | Vitesse |
//                                                                                                                                           |          |         |  07  |  |                |         |
//                                                                                                                                           |          |  |------|      |  |                |         |
#define VL             1    //  VL  fixe                                  (Voie Libre, vert ou blanc suivant l'endroit où il est implanté)   |     1    |  |  06     06  |  |  03 ou 04      |    Maxi |
#define A              2    //   A  fixe                                  (Avertissement)                                                    |     2    |  |             |  |  01            |    Maxi |
#define S              8    //   S  fixe                                  (Sémaphore)                                                        |     8    |  |  05     07  |  |  02            |   Arrêt |
#define C              9    //   C  fixe                                  (Carré, rouge ou violet suivant l'endroit où il est implanté)      |     9    |  |      |------|  |  05 et 02      |   Arrêt |
#define VL_R_C        11    //  VL  fixe + R  clignotant                  (Voie Libre + Ralentissement 60)                                   |    11    |  |  04  |         |  03 et 06      | 60 km/h |
#define A_R_C         12    //   A  fixe + R  clignotant                  (Avertissement + Ralentissement 60)                                |    12    |  |      |         |  01 et 06      | 60 km/h |
#define VL_R          21    //  VL  fixe + R  fixe                        (Voie Libre + Ralentissement 30)                                   |    21    |  |  03  |         |  03 et 06      | 30 km/h |
#define A_R           22    //   A  fixe + R  fixe                        (Avertissement + Ralentissement 30)                                |    22    |  |      |         |  01 et 06      | 30 km/h |
#define VL_RR_C      101    //  VL  fixe + RR clignotant                  (Voie Libre + Rappel de Ralentissement 60)                         |   101    |  |  02  |         |  03 et 07      |    Maxi |
#define A_RR_C       102    //   A  fixe + RR clignotant                  (Avertissement + Rappel de Ralentissement 60)                      |   102    |  |      |         |  01 et 07      |    Maxi |
#define VL_R_C_RR_C  111    //  VL  fixe + R  clignotant + RR clignotant  (Voie Libre + Ralentissement 60 + Rappel de Ralentissement 30)     |   111    |  |  01  |         |  03 et 07      | 60 km/h |
#define A_R_C_RR_C   112    //   A  fixe + R  clignotant + RR clignotant  (Avertissement + Ralentissement 60 + Rappel de Ralentissement 60)  |   112    |  |------|         |  01 et 07      | 60 km/h |
#define VL_R_RR_C    121    //  VL  fixe + R  fixe       + RR clignotant  (Voie Libre + Ralentissement 30 + Rappel de Ralentissement 60)     |   121    |                   |  03 et 07      | 30 km/h |
#define A_R_RR_C     122    //   A  fixe + R  fixe       + RR clignotant  (Avertissement + Ralentissement 30 + Rappel de Ralentissement 60)  |   122    |                   |  01 et 07      | 30 km/h |
#define VL_RR        201    //  VL  fixe + RR fixe                        (Voie Libre + Rappel de Ralentissement 30)                         |   201    |                   |  03 et 06      |    Maxi |
#define A_RR         202    //   A  fixe + RR fixe                        (Avertissement + Rappel de Ralentissement 30)                      |   202    |                   |  01 et 06      |    Maxi |
#define VL_R_C_RR    211    //  VL  fixe + R  clignotant + RR fixe        (Voie Libre + Ralentissement 60 + Rappel de Ralentissement 30)     |   211    |                   |  03 et 07      | 60 km/h |
#define A_R_C_RR     212    //   A  fixe + R  clignotant + RR fixe        (Avertissement + Ralentissement 60 + Rappel de Ralentissement 30)  |   212    |                   |  01 et 07      | 60 km/h |
#define VL_R_RR      221    //  VL  fixe + R  fixe       + RR fixe        (Voie Libre + Ralentissement 30 + Rappel de Ralentissement 30)     |   221    |                   |  03 et 07      | 30 km/h |
#define A_R_RR       222    //   A  fixe + R  fixe       + RR fixe        (Avertissement + Ralentissement 30 + Rappel de Ralentissement 30)  |   222    |                   |  01 et 07      | 30 km/h |

