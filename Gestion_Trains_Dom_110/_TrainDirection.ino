    //-------------------------- Determining the direction of the trains -----------------------------------
    //  To find the direction of the train in DCC, without decoding DCC or XPressNet, we follow the train.
    //  Ther is three zones in a Block : stop zone clockwise, stop zone conterclockwise and the plain track, between the other zones.
    //  According to the various occupations of the zones, we can determine the direction of travel in only one block.
   
    //  Block::DeterminingTrainsDirections();
    //  This part of the sketch must be moved to a block-module
  
//--------------------------------------------- OccupancyZonesTest() --------------------------------------------------

void Block::OccupancyZonesTest(boolean aClw_stop_zone_status, boolean aPlain_track_status, boolean aCclw_stop_zone_status)
{
    //-------------------test_000-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == FREE))
    {
        test_000 = true;
    }
    else
    {
        test_000 = false;
    }
    //-------------------test_001-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == BUSY))
    {
        test_001 = true;
    }
    else
    {
        test_001 = false;
    }
    //-------------------test_010-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == FREE))
    {
        test_010 = true;
    }
    else
    {
        test_010 = false;
    }
    //-------------------test_011-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == BUSY))
    {
        test_011 = true;
    }
    else
    {
        test_011 = false;
    }
    //-------------------test_100-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == FREE))
    {
        test_100 = true;
    }
    else
    {
        test_100 = false;
    }
    //-------------------test_101-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == BUSY))
    {
        test_101 = true;
    }
    else
    {
        test_101 = false;
    }
    //-------------------test_110-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == FREE))
    {
        test_110 = true;
    }
    else
    {
        test_110 = false;
    }
    //-------------------test_111-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == BUSY))
    {
        test_111 = true;
    }
    else
    {
        test_111 = false;
    }
}

//--------------------------------------------- OccupancyZonesMemo() --------------------------------------------------

void Block::OccupancyZonesMemo(boolean aClw_stop_zone_status, boolean aPlain_track_status, boolean aCclw_stop_zone_status)
{
    //-------------------memo_000-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == FREE))
    {
        memo_000 = true;
    }
    else
    {
        memo_000 = false;
    }
    //-------------------memo_001-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == BUSY))
    {
        memo_001 = true;
    }
    else
    {
        memo_001 = false;
    }
    //-------------------memo_010-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == FREE))
    {
        memo_010 = true;
    }
    else
    {
        memo_010 = false;
    }
    //-------------------memo_011-----------------
    if ((aClw_stop_zone_status == FREE) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == BUSY))
    {
        memo_011 = true;
    }
    else
    {
        memo_011 = false;
    }
    //-------------------memo_100-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == FREE))
    {
        memo_100 = true;
    }
    else
    {
        memo_100 = false;
    }
    //-------------------memo_101-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == FREE) && (aCclw_stop_zone_status == BUSY))
    {
        memo_101 = true;
    }
    else
    {
        memo_101 = false;
    }
    //-------------------memo_110-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == FREE))
    {
        memo_110 = true;
    }
    else
    {
        memo_110 = false;
    }
    //-------------------memo_111-----------------
    if ((aClw_stop_zone_status == BUSY) && (aPlain_track_status == BUSY) && (aCclw_stop_zone_status == BUSY))
    {
        memo_111 = true;
    }
    else
    {
        memo_111 = false;
    }
}

//------------------------------------- Advance() -----------------------------------------------------

void Block::Advance()
{
    if ((memo_000 == BUSY) && (test_001 == BUSY))
    {
        block_direction = CLOCKWISE;
    }
    else
    {
        if ((memo_001 == BUSY) && (test_011 == BUSY))
        {
            block_direction = CLOCKWISE;
        }
        else
        {
            if ((memo_011 == BUSY) && (test_010 == BUSY))
            {
                block_direction = CLOCKWISE;
            }
            else
            {
                if ((memo_010 == BUSY) && (test_110 == BUSY))
                {
                    block_direction = CLOCKWISE;
                }
                else
                {
                    if ((memo_110 == BUSY) && (test_100 == BUSY))
                    {
                        block_direction = CLOCKWISE;
                    }
                    else
                    {
                        if ((memo_100 == BUSY) && (test_000 == BUSY))
                        {
                            block_direction = CLOCKWISE;
                        }
                        else
                        {
                            if ((memo_000 == BUSY) && (test_100 == BUSY))
                            {
                                block_direction = CONTERCLOCKWISE;
                            }
                            else
                            {
                                if ((memo_100 == BUSY) && (test_110 == BUSY))
                                {
                                    block_direction = CONTERCLOCKWISE;
                                }
                                else
                                {
                                    if ((memo_110 == BUSY) && (test_010 == BUSY))
                                    {
                                        block_direction = CONTERCLOCKWISE;
                                    }
                                    else
                                    {
                                        if ((memo_010 == BUSY) && (test_011 == BUSY))
                                        {
                                            block_direction = CONTERCLOCKWISE;
                                        }
                                        else
                                        {
                                            if ((memo_011 == BUSY) && (test_001 == BUSY))
                                            {
                                                block_direction = CONTERCLOCKWISE;
                                            }
                                            else
                                            {
                                                if ((memo_001 == BUSY) && (test_000 == BUSY))
                                                {
                                                    block_direction = CONTERCLOCKWISE;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//----------------------- DeterminingTrainsDirections() -------------------------------------------------

void Block::DeterminingTrainsDirections()
{  
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
        Block::OccupancyZonesTest (blockList[i].getClwStopZoneStatus0(),blockList[i].getPlainTrackStatus0(),blockList[i].getCclwStopZoneStatus0());
        //  current position of the train
         
        Block::OccupancyZonesMemo (blockList[i].getClwStopZoneStatus(), blockList[i].getPlainTrackStatus(), blockList[i].getCclwStopZoneStatus());
        //  previous position of the train (stored)
         
        if ((blockList[i].getClwStopZoneStatus0()  != blockList[i].getClwStopZoneStatus())
        ||  (blockList[i].getPlainTrackStatus0()   != blockList[i].getPlainTrackStatus())
        ||  (blockList[i].getCclwStopZoneStatus0() != blockList[i].getCclwStopZoneStatus()))
        {
            //  the train has mouved
            Block::Advance();
            blockList[i].setTrainDirection(block_direction);
          
            if (test_000 == true)
            {
                if (blockList[i].getTrainDirection() == CLOCKWISE)
                {
                    blockList[blockList[i].getNextBlock()-101].setTrainDirection(CLOCKWISE);
                }
                else
                {
                    blockList[blockList[i].getPreviousBlock()-101].setTrainDirection(CONTERCLOCKWISE);
                }
            }
        }
        if (test_000 == false)
        {
            blockList[i].setBusy(BUSY);
        }
        else
        {
            blockList[i].setBusy(FREE);
        }
          
        //  storage of the position of the train
        blockList[i].setClwStopZoneStatus (blockList[i].getClwStopZoneStatus0()); 
        blockList[i].setPlainTrackStatus  (blockList[i].getPlainTrackStatus0());
        blockList[i].setCclwStopZoneStatus(blockList[i].getCclwStopZoneStatus0());
    }
}
