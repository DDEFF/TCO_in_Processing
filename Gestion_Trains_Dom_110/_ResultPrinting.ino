//------------------------- Results printing -----------------------
void   result_printing()
{  

    Serial.println();
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
        blockList[i].PrintBlock();
    }

    Serial.println();  
    for (byte i = 0; i < MAX_TRAIN; i++)
    {
        trainList[i].PrintTrain();
    }

    Serial.println();  
    for (byte i = 0; i < MAX_TURNOUT; i++)
    {
        turnoutList[i].PrintTurnout();
    }
       
    Serial.println();
    Serial.println("Turnout path :");
    for (byte i=0; i < MAX_TURNOUT; i++)
    {
        Serial.println();
        Serial.println(turnoutList[i].getCurrentTurnout());
        Serial.println(turnoutList[i].getTurnoutPath());
    }

    Serial.println();
    Serial.println("Wanted routes and turnouts positions :");
    for (byte i=0; i < MAX_W_ROUTE; i++)
    {
        Serial.println();
        w_routeList[i].PrintW_Route();
    }

    Serial.println();
    Serial.println("Reserved routes and turnouts positions :");
    for (byte i=0; i < MAX_R_ROUTE; i++)
    {
        Serial.println();
        r_routeList[i].PrintR_Route();
    }
  
    Serial.println();
    Serial.println("Made routes, depending of turnouts positions");
    for (byte i=0; i < MAX_ROUTE; i++)
    {
        Serial.println();
        routeList[i].PrintRoute();
    }

}
