#include "Block.h"
#include "Turnout.h"
#include "Route.h"
#include "R_Route.h"
#include "W_Route.h"
#include "Train.h"

#include "Define.h"

//------------------------------ Global variables --------------------------------------
//  Nota : in the names of variables, "clw" = "clockwise" and "cclw" = "conterclockwise"

byte block_line;
byte turnout_line;
byte route_line;
byte r_route_line;
byte w_route_line;
byte total_train = -1;
byte t_block_line;
byte t_turnout_line;

byte current_signal;
byte found_turnout;
boolean found_train;

byte origine_block;
byte extremity_block;
byte first_turnout;

byte final_level;
byte current_element;
byte current_priority;
byte total_pin_route;

boolean current_turnout_path;
boolean route_direction;

unsigned long last_route_date = 0;
unsigned long dateCourante;
unsigned long intervalle;
unsigned long dateDernierChangement;

boolean aClw_stop_zone_status  = FREE;
boolean aCclw_stop_zone_status = FREE;
boolean aPlain_track_status    = FREE;

boolean test_000;
boolean test_001;
boolean test_010;
boolean test_011;
boolean test_100;
boolean test_101;
boolean test_110;
boolean test_111;
boolean memo_000;
boolean memo_001;
boolean memo_010;
boolean memo_011;
boolean memo_100;
boolean memo_101;
boolean memo_110;
boolean memo_111;
boolean block_direction;

byte pass = 0;
byte pass_direction = 0;

byte possible_route[4][50];
byte pin_route_j1;
byte pin_route_j2;
byte t_block_line0;
byte t_block_line1;
byte t_last_block;

boolean train_dir;
boolean turnout_passed;
int t_delay;
//---------------------------- Block -----------------------------
// current_block must be in [101 - 255] ==> MAX_BLOCK <= 155

Block blockList[MAX_BLOCK] =
{   Block(  0,101,102, 2),
    Block(101,102,103, 2),
    Block(102,103, 19, 2),
    Block( 19,104,  0, 2),
    Block(  1,105,  3, 4),
    Block(  1,106,  4, 6),
    Block(  2,107,  5, 8),
    Block(  2,108,  5,10),
    Block(  4,109,111, 6),
    Block(  5,110,112, 8),
    Block(109,111,113, 6),
    Block(110,112,114, 8),
    Block(111,113,116, 6),
    Block(112,114,115, 8),
    Block(114,115,117, 8),
    Block(113,116,118, 6),
    Block(115,117,  6, 8),
    Block(116,118,  8, 6),
    Block(  0,119, 18, 4),
    Block(  0,120, 18, 2),
    Block(  0,121, 17, 0),
    Block( 17,122, 10, 4),
    Block(  7,123, 15,10),
    Block(  7,124, 16, 8),
    Block(  9,125, 14, 6),
    Block( 11,126, 12, 4),
    Block( 11,127, 12, 2),
    Block( 15,128,  0,10),
    Block( 13,129,  0, 4),
    Block( 21,130,  1, 6),
    Block( 20,131,  2, 8)
};

//------------------------------ Turnout ---------------------------
// current_turnout must be in [1 - 100] ==> MAX_TURNOUT <= 100

Turnout turnoutList[MAX_TURNOUT] =
{   Turnout(130,  1,106,105, 6, 5, 30),
    Turnout(131,  2,107,108, 8, 9, 60),
    Turnout(105,  3,  4, 19, 4, 3, 30),
    Turnout(109,  4,106,  3, 6, 5, 30),
    Turnout(110,  5,107,108, 8, 9, 60),
    Turnout(117,  6,  7,  8, 8, 7, 30),
    Turnout(  6,  7,124,123, 8, 9, 30),
    Turnout(  9,  8,118,  6, 6, 7, 30),
    Turnout(  8,  9,125, 10, 6, 5, 30),
    Turnout( 11, 10,122,  9, 4, 5, 30),
    Turnout( 10, 11,126,127, 4, 3, 30),
    Turnout( 13, 12,126,127, 4, 3, 30),
    Turnout( 12, 13,129, 14, 4, 5, 30),
    Turnout( 21, 14,125, 13, 6, 5, 30),
    Turnout(123, 15,128, 16,10, 9, 30),
    Turnout( 20, 16,124, 15, 8, 9, 30),
    Turnout(122, 17, 18,121, 4, 0, 30),
    Turnout( 17, 18,119,120, 4, 2, 30),
    Turnout(104, 19,103,  3, 2, 3, 30),
    Turnout(131, 20, 16, 21, 8, 7, 30),
    Turnout( 14, 21,130, 20, 6, 7, 30)
};

//-------------------------- W_Route ------------------------------------

W_Route w_routeList[MAX_W_ROUTE];

//--------------------------- R_Route ----------------------------------------

R_Route r_routeList[MAX_R_ROUTE];

//-------------------------- Priorities in r_route --------------------------------

void Priorities()  //  Priorities is the order in which routes are calculated 
{
    r_routeList[ 0].setR_Priority(130);  r_routeList[ 0].setR_First( 1);
  
    r_routeList[ 1].setR_Priority(131);  r_routeList[ 1].setR_First( 2);
  
    r_routeList[ 2].setR_Priority(109);  r_routeList[ 2].setR_First( 4);
    r_routeList[ 3].setR_Priority(104);  r_routeList[ 3].setR_First(19);
    
    r_routeList[ 4].setR_Priority(110);  r_routeList[ 4].setR_First( 5);
  
    r_routeList[ 5].setR_Priority(117);  r_routeList[ 5].setR_First( 6);
    
    r_routeList[ 6].setR_Priority(118);  r_routeList[ 6].setR_First( 8);
  
    r_routeList[ 7].setR_Priority(122);  r_routeList[ 7].setR_First(17);
  
    r_routeList[ 8].setR_Priority(131);  r_routeList[ 8].setR_First(20);
    r_routeList[ 9].setR_Priority(130);  r_routeList[ 9].setR_First(21);
    r_routeList[10].setR_Priority(128);  r_routeList[10].setR_First(15);
    r_routeList[11].setR_Priority(129);  r_routeList[11].setR_First(13);
}
//--------------------------- Route ------------------------------------------

Route routeList[MAX_ROUTE];

byte new_route_init[] = {130,1, 131,2, 103,19, 105,3, 106,4, 107,5, 108,5, 117,6, 118,8, 119,18, 120,18, 121,17, 123,15, 124,16, 125,14, 126,12, 127,12};

//--------------------------- Pins ---------------------------------------

boolean pin_route[MAX_W_ROUTE];

//--------------------------- Train ----------------------------------------

Train trainList[MAX_TRAIN];

//****************************************************************************************************
//**                                             Setup()                                            **
//****************************************************************************************************

void setup()
{
    Serial.begin(115200);
    //----------------------------- Reset Wanted Route ------------------------------------------------
    // W_Routes is an alive table, constantly recalculated according to the wanted routes.
    
    for (byte i = 0; i < MAX_W_ROUTE; i++)
    {
        w_routeList[i].ResetW_Route();
    }
    W_Route::W_LineFirst();
  
    //----------------------------- Reset Reserved Route ------------------------------------------------
    // R_Routes is an alive table, constantly recalculated according to the reserved routes.
    
    for (byte i = 0; i < MAX_R_ROUTE; i++)
    {
        r_routeList[i].ResetR_Route();
    }  
    Priorities();
  
    //----------------------------- Reset Route ------------------------------------------------  
  
    for (byte i = 0; i < MAX_ROUTE; i++)
    {
        routeList[i].ResetRoute();
    }
    
    //----------------------------- Reset Train ------------------------------------------------  
  
    for (byte i = 0; i < MAX_TRAIN; i++)
    {
        trainList[i].ResetTrain();
    }
}

//**************************************************************************************************
//**                                               loop()                                         **
//**************************************************************************************************
void loop()
{    
    unsigned long dateDernierChangement = micros();  
  
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
    //  ++    Il y a 2 x 9 x 12 = 216 itinéraires de la gare                                              ++
    //  ++    et l'Arduino trouve les 36 non possibles (il en reste donc 180) :                           ++
    //  ++    Je teste plusieurs situations simultannées, par des "copier-coller" d'essais,               ++
    //  ++    mais il n'en restera qu'une, gérée par les BP.                                              ++
    //  ++    Je teste un BP tout seul appuyé : il efface l'itinéraire demandé et éventuellement réservé  ++
    //  ++    Puis je teste 2 BP et c'est donc l'itinéraire demandé, puis réservé, puis réalisé           ++
    //  ++    quand les conditions sont remplies.                                                         ++
    //  ++    Cette partie "essais" est donc vouée à disparaitre                                          ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
    train01();
            
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
    //  ++                                     Suite du programme                                         ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    //-----------------------------W_Route filling -------------------------------------------------------
  
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  ++    Les itinéraires sont demandés via un BP origine et un BP extrémité sur le TCO.              ++
    //  ++    Le sens de l'itinéraire est demandé par l'ordre d'appui sur les BP.                         ++
    //  ++    Cette partie utilise le bus CAN n°2, mais n'est pas réalisée.                               ++
    //  ++    Mais le programme est donné ci-après entre les * *                                          ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    //--------------------------------- Exchange of the reserved routes --------------------------------------------------
    //  Calculation of all the w_route with status FREE
  
    for (byte i = 0; i < MAX_W_ROUTE; i++)
    {
        w_routeList[i].W_RouteStatusFree();
    }
  
    //  Calculation of all the w_route status, in the order of priorities
    for (byte i = 0; i < MAX_R_ROUTE; i++)
    {
        current_priority = r_routeList[i].getR_Priority();
    
        for (byte j = 0; j < MAX_W_ROUTE; j++)
        {
            if ((w_routeList[j].getW_StartBlock() == current_priority) || (w_routeList[j].getW_EndBlock() == current_priority))
            {
                if (w_routeList[j].getW_RouteStatus() == FREE)
                {
                    w_routeList[j].W_RouteStatus();
                }
            }
        }
    }
   
    for (byte i = 0; i < MAX_W_ROUTE; i++)
    {
        if (w_routeList[i].getW_RouteStatus() == RESERVED)
        //  Only the routes whose status is RESERVED can be exchanged between w_route and r_route
        {
          w_route_line = i;  //  It's #line of the route to exchange in W_Route
            for (byte j = 0; j < MAX_R_ROUTE; j++)
            {
                if ((r_routeList[j].getR_Priority() == w_routeList[w_route_line].getW_StartBlock()) && (r_routeList[j].getR_First() == w_routeList[w_route_line].getW_Turnout(0)))
                {
                    r_route_line = j;  // It's #line of the route to exchange in R_Route
                    r_routeList[r_route_line].R_RouteExchange();
                }
            }
    
          for (byte j = 0; j < MAX_R_ROUTE; j++)
            {
                if ((r_routeList[j].getR_Priority() == w_routeList[w_route_line].getW_EndBlock()) && (r_routeList[j].getR_First() == w_routeList[w_route_line].getW_Last())) 
                {
                    r_route_line = j;  // It's #line of the route to exchange in R_Route
                    r_routeList[r_route_line].R_RouteExchange();
                }
            }
        }
    }
  
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  ++    L'Arduino DUE envoie toutes les positions aux aiguilles données                             ++
    //  ++    par la Classe des itinéraires réservés.                                                     ++
    //  ++    Cette partie utilise le bus CAN n°2, mais n'est pas réalisée.                               ++
    //  ++    Je vais ici forcer les positions dans la Classe Turnout.                                    ++
    //  ++    C'est l'objet du process suivant (voué, donc, à disparaître) :                              ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
    //----------------------------------- Put in position of turnouts -------------------------------------------
    
    for (byte i = 0; i < MAX_R_ROUTE; i++)
    {
        r_routeList[i].PutTurnoutPosition();    
    } 
    
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  ++    L'Arduino DUE récupère toutes les positions réélles des aiguilles données par le réseau.    ++
    //  ++    Cette partie utilise le bus CAN n°2, mais n'est pas réalisée.                               ++
    //  ++    Ces positions sont mises à jour dans la Classe Turnout.                                     ++
    //  ++    Il reste à essayer tous les itinéraires pour savoir lesquels sont réalisés.                 ++
    //  ++    C'est l'objet du process suivant :                                                          ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
    //------------------------ Reset Route ---------------------------
    // Routes is an alive table, constantly recalculated according to the positions of turnouts.
    // So, it must be reset in the loop(), not in the setup().
  
    for (byte i = 0; i < MAX_ROUTE; i++)
    {
        routeList[i].ResetRoute();
    }
    
    //------------------------- Route filling -------------------------  
  
    for (byte i = 0; i < 2*MAX_ROUTE; i = i+2)
    {
        route_line = i/2;
        routeList[route_line].NewRouteInit(new_route_init[i],new_route_init[i+1]);
        routeList[route_line].TurnoutFilling();    
    }
  
    //--------------------- Next Block --------------------------
   
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
        blockList[i].NextBlock();
    }
  
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  ++                                    SIGNALS                                                     ++
    //  ++    The author being French, it is the french signalling which will be described                ++
    //  ++    The explanations of the french signals are in the tab define.h                              ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    Block::SignalsFilling();
  
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  ++                                                                                                ++
    //  ++                     TRAINS : THE CASE OF THE DC : use the LOCODUINO cards                      ++
    //  ++                                                                                                ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  ++                                                                                                ++
    //  ++                                TRAINS : THE CASE OF THE DCC                                    ++
    //  ++                                                                                                ++
    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    //---------------------- Finding isolated trains --------------------------------------------
    //  The problem is to attribuate a line for each train in the trainList[]
    //  When a train is only on one block, whe create a new line in the trainList[]
    //  But if the block is already followed, we don't create a new line.
      
    Train::FindingIsolatedTrains();
    
    //----------------------------- UpdateTrainList() ---------------------------
    //  Because trains move, we must update the trainList[]
  
    Train::UpdateTrainList();
  
    //---------------------- Problem of trains in opposition ----------------------
    //  If two Cn+2 Blocks are identical, it is because we have two trains which go in opposite sense there. The second has to have a "calculated" semaphore.
  
    Train::OppositeTrains();
    
    //------------------ Problem of the three blocks ---------------------------------
    //  A train cann't occupy simultaneously three blocks : there is a lost wagon
    
    Train::ThreeBlocks();
    
//    dateCourante = micros();
//    intervalle = dateCourante - dateDernierChangement;
    
//    Serial.println();
//    Serial.println(intervalle);
//     result_printing();
}




