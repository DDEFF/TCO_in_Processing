#include "Route.h"

#include "Variables.h"
#include <Arduino.h>

#include "Block.h"
extern Block blockList[];

#include "Turnout.h"
extern Turnout turnoutList[];

#include "R_Route.h"
extern R_Route r_routeList[];

//-----------------------------
Route::Route(byte aStart_block, byte aTurnout[MAX_TURNOUT_ROUTE], byte aEnd_block, byte aMax_route_speed)
{
    this->start_block          = aStart_block;
    
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        this->turnout[i]         = aTurnout[i];
    }     
    this->end_block            = aEnd_block;
    this->max_route_speed      = aMax_route_speed;
}

//**************************** Other functions ******************************************

//------------------------------ ResetRoute -------------------------------------

void Route::ResetRoute ()
{
  this->start_block          = 0;  
  for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
  {
      this->turnout[i] = 0; 
  }     
  this->end_block            = 0;
  this->max_route_speed      = 0;
}

//------------------------- PrintRoute --------------------------

void Route::PrintRoute ()
{
//  if (this->start_block > 0)
//  {
        Serial.print(this->start_block);
        Serial.print(" ");
        for (int i = 0; i < MAX_TURNOUT_ROUTE; i++)
        {
            if (this->turnout[i] <10)
            {
                Serial.print(" ");
            }
            Serial.print(this->turnout[i]);
            Serial.print(" ");
        }
        Serial.print(this->end_block);
        Serial.print(" ");
        Serial.print(this->max_route_speed);
//  }
}

//----------------------------- NewRouteInit ------------------------------------------
//  New route tests the real position of turnouts. First : initialization.

void Route::NewRouteInit(byte aBlock, byte aTurnout)
{
    for (byte i = 0; i < MAX_R_ROUTE; i++)
    {
        if (aBlock == r_routeList[i].getR_StartBlock())
        {  
            if (((blockList[aBlock-101].getCurrentBlock() == turnoutList[aTurnout-1].getStraightPath())  && (turnoutList[aTurnout-1].getTurnoutPath() == false))
            ||  ((blockList[aBlock-101].getCurrentBlock() == turnoutList[aTurnout-1].getDivergingPath()) && (turnoutList[aTurnout-1].getTurnoutPath() == true))
            ||   (blockList[aBlock-101].getCurrentBlock() == turnoutList[aTurnout-1].getNativeSide()))
            {
                //  turnout[0] is in the good path
                this->start_block     = aBlock;
                this->turnout[0]      = aTurnout;
                this->max_route_speed = 160;
                if (turnoutList[aTurnout-1].getTurnoutPath() == true)
                  this->max_route_speed = turnoutList[aTurnout-1].getTurnoutMaxSpeed();         
                break;
            }
        }
        if ((aBlock == r_routeList[i].getR_EndBlock()) && (aTurnout == r_routeList[i].getR_First()))
        {  
            if (((blockList[aBlock-101].getCurrentBlock() == turnoutList[aTurnout-1].getStraightPath())  && (turnoutList[aTurnout-1].getTurnoutPath() == false))
            ||  ((blockList[aBlock-101].getCurrentBlock() == turnoutList[aTurnout-1].getDivergingPath()) && (turnoutList[aTurnout-1].getTurnoutPath() == true))
            ||   (blockList[aBlock-101].getCurrentBlock() == turnoutList[aTurnout-1].getNativeSide()))
            {
              //  turnout[0] is in the good path
              this->start_block     = aBlock;
              this->turnout[0]      = aTurnout;
              this->max_route_speed = 160;
              if (turnoutList[aTurnout-1].getTurnoutPath() == true)
                this->max_route_speed = turnoutList[aTurnout-1].getTurnoutMaxSpeed();         
              break;
            }
        }
    }
}

//------------------------------- AddTurnout -------------------------------------------
//   Add turnouts in real positions until you find a block

void Route::AddTurnout(byte aTurn01,byte aTurn02)
{
    
    if (aTurn01 == turnoutList[aTurn02-1].getNativeSide())
    {
        if (turnoutList[aTurn02-1].getTurnoutPath() == STRAIGHT)
        {
            this->end_block = turnoutList[aTurn02-1].getStraightPath();
        }
        else
        {
            this->end_block = turnoutList[aTurn02-1].getDivergingPath();
            this->max_route_speed = min(turnoutList[aTurn02-1].getTurnoutMaxSpeed(), this->max_route_speed);
        }
    }
    else
    {
        if ((aTurn01 == turnoutList[aTurn02-1].getStraightPath()) || (aTurn01 == turnoutList[aTurn02-1].getDivergingPath()))
        {
            this->end_block = turnoutList[aTurn02-1].getNativeSide();
            if (aTurn01 == turnoutList[aTurn02-1].getDivergingPath())
            {
                this->max_route_speed = min(turnoutList[aTurn02-1].getTurnoutMaxSpeed(), this->max_route_speed);
            }
        }
        else
        {
            Serial.println("Problem in initialization of Turnout class or in aBlock/aTurnout in NewRouteInit");
        }
    }
    if (this->end_block <101)  // it's not the end of the route because it's a turnout 
    {
        if (((aTurn02 == turnoutList[this->end_block-1].getStraightPath())  && (turnoutList[this->end_block-1].getTurnoutPath() == false))
        ||  ((aTurn02 == turnoutList[this->end_block-1].getDivergingPath()) && (turnoutList[this->end_block-1].getTurnoutPath() == true))
        ||   (aTurn02 == turnoutList[this->end_block-1].getNativeSide()))
        {
            if ((aTurn01 < 101) && (turnoutList[aTurn01-1].getTurnoutPath() == DIVERGING))
            {
                this->max_route_speed = min(turnoutList[aTurn01-1].getTurnoutMaxSpeed(), this->max_route_speed);
            }
            if ((aTurn02 < 101) && (turnoutList[aTurn02-1].getTurnoutPath() == DIVERGING))
            {
                this->max_route_speed = min(turnoutList[aTurn02-1].getTurnoutMaxSpeed(), this->max_route_speed);
            }
        }
        else
        {
            ResetRoute();
        }   
    }
}

//---------------------------------- TurnoutFilling --------------------------------------------------

void Route::TurnoutFilling()
{
    if (this->start_block > 0)  // Route is really initialized
    {
        AddTurnout(this->start_block, turnout[0]);
    
        for (byte i=1; i<MAX_TURNOUT_ROUTE; i++)
        {
            if ((this->end_block != 0) && (this->end_block < 101))  // turnout[i] isn't the last turnout
            {
                this->turnout[i] = end_block;
                AddTurnout(this->turnout[i-1],this->turnout[i]);
            }
            else
            {
                break;
            }      
        }
        if ((this->end_block != 0) && (this->end_block < 101))  // The route is too big
        {
            this->turnout[MAX_TURNOUT_ROUTE-1] = this->end_block;
            Serial.print("Your train station is too big : the maximum number of turnouts for a single route is ");
            Serial.print(MAX_TURNOUT_ROUTE);
        }                                                         
    }
}
