#include "Define.h"
#include "Variables.h"

//------------------------------- Train01() -------------------------------------------
void train01()
{
    //  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
              byte    def_01[]  = { 105, 0, 109, 111, 113, 116, 118, 0, 127, 0, 130, 0, 105 };         //  Always in clockwise direction
              boolean train_dir = CLOCKWISE;                                                           //  CLOCKWISE or CONTERCLOCKWISE
              int     t_delay   = 1000;                                                                //  "speed" of the train 
    //  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    const int num_elements   = (sizeof(def_01) / sizeof(def_01[0]));
    int       block_order    = 0;
    boolean   turnout_passed = false;

    if (!train_dir)
    {
        //------------------------------------------------ train goes clockwise --------------------------------------------------       
        for (byte i = 0; i<num_elements-1; i++)
        {
            if (def_01[i] != 0)
            //----------------- train is on a block ------------------------------------
            {
                block_order = block_order+1;
                t_block_line = def_01[i]-101;                                   //  block line
                blockList[t_block_line].setTrainDirection(train_dir);           //  update block direction

                switch (block_order % 2)
                {
                    case 0:
                        t_block_line0 = t_block_line;
                        blockList[t_block_line0].setBusy(BUSY);                  //  update current block busy
                        blockList[t_block_line0].setTrainDirection(0);           //  update direction CLOCKWISE
                        calculOutValue(def_01[i], 0, blockList[t_block_line0].getTrainDirection(), blockList[t_block_line0].getBusy(), 3);
                       
                        blockList[t_block_line1].setBusy(BUSY);                  //  update previous block busy
                        blockList[t_block_line1].setTrainDirection(0);           //  update direction CLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line1].getTrainDirection(), blockList[t_block_line1].getBusy(), 1);
                        delay(t_delay); 
                       
                        blockList[t_block_line1].setBusy(FREE);                  //  update previous block free
                        blockList[t_block_line1].setTrainDirection(0);           //  update direction CLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line1].getTrainDirection(), blockList[t_block_line1].getBusy(), 0);
                        delay(t_delay); 

                        if (i == 0)
                        {
                            erase_first_route();
                        }
                        erase_turnout_passed();
                        t_last_block = i;
                        break;
                    case 1:
                        t_block_line1 = t_block_line;
                        blockList[t_block_line1].setBusy(BUSY);                  //  update current block busy
                        blockList[t_block_line1].setTrainDirection(0);           //  update direction CLOCKWISE
                        calculOutValue(def_01[i], 0, blockList[t_block_line1].getTrainDirection(), blockList[t_block_line1].getBusy(), 3); 
                        
                        blockList[t_block_line0].setBusy(BUSY);                  //  update previous block busy
                        blockList[t_block_line0].setTrainDirection(0);           //  update direction CLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line0].getTrainDirection(), blockList[t_block_line0].getBusy(), 1);
                        delay(t_delay); 
                       
                        blockList[t_block_line0].setBusy(FREE);                  //  update previous block free
                        blockList[t_block_line0].setTrainDirection(0);           //  update direction CLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line0].getTrainDirection(), blockList[t_block_line0].getBusy(), 0);                       
                        delay(t_delay);
                        
                        if (i == 0)
                        {
                            erase_first_route();
                        }
                        erase_turnout_passed();
                        t_last_block = i;                       
                        break;
                }
            }
            else
            {
                //----------------------------- train is on turnouts ---------------------------                                         
                for (byte j = 0; j<50; j++)
                {
                    if ((def_01[i-1] == possible_route[1][j]) && (possible_route[3][j] == 0))
                    {                        
                        pin_route_j1 = j;
                        pin_route[j] = true;
                        R_Route::RouteChoice();   //  first button
                        break;
                    }
                }
                for (byte j = 0; j<50; j++)
                {
                    if ((def_01[i+1] == possible_route[1][j]) && (possible_route[3][j] == 1))
                    {                        
                        pin_route_j2 = j;
                        pin_route[j] = true;
                        R_Route::RouteChoice();   //  secound button
                        break;
                    }
                }
                drawing_routes();
            }
        }
    }
    else
    {
        //--------------------------------------------- train goes conter clockwise ---------------------------------------------------
        for (byte i = num_elements-1; i>0; i--)
        {
            if (def_01[i] != 0)
            //----------------- train is on a block ------------------------------------
            {
                block_order = block_order+1;
                t_block_line = def_01[i]-101;                                   //  block line
                blockList[t_block_line].setTrainDirection(train_dir);           //  update block direction

                switch (block_order % 2)
                {
                    case 0:
                        t_block_line0 = t_block_line;
                        blockList[t_block_line0].setBusy(BUSY);                  //  update current block busy
                        blockList[t_block_line0].setTrainDirection(1);           //  update direction COUNTERCLOCKWISE
                        calculOutValue(def_01[i], 0, blockList[t_block_line0].getTrainDirection(), blockList[t_block_line0].getBusy(), 3);
                                               
                        blockList[t_block_line1].setBusy(BUSY);                  //  update previous block busy
                        blockList[t_block_line1].setTrainDirection(1);           //  update direction COUNTERCLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line1].getTrainDirection(), blockList[t_block_line1].getBusy(), 1);
                        delay(t_delay);
                        
                        blockList[t_block_line1].setBusy(FREE);                  //  update previous block free
                        blockList[t_block_line1].setTrainDirection(1);           //  update direction COUNTERCLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line1].getTrainDirection(), blockList[t_block_line1].getBusy(), 0);
                        delay(t_delay);

                        if (i == num_elements-1)
                        {
                            erase_first_route();
                        }
                        erase_turnout_passed();
                        t_last_block = i;
                        break;
                    case 1:
                        t_block_line1 = t_block_line;
                        blockList[t_block_line1].setBusy(BUSY);                  //  update current block busy
                        blockList[t_block_line1].setTrainDirection(1);           //  update direction COUNTERCLOCKWISE
                        calculOutValue(def_01[i], 0, blockList[t_block_line1].getTrainDirection(), blockList[t_block_line1].getBusy(), 3);
                        
                        blockList[t_block_line0].setBusy(BUSY);                  //  update previous block busy
                        blockList[t_block_line0].setTrainDirection(1);           //  update direction COUNTERCLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line0].getTrainDirection(), blockList[t_block_line0].getBusy(), 1);
                        delay(t_delay);
                        
                        blockList[t_block_line0].setBusy(FREE);                  //  update previous block free
                        blockList[t_block_line0].setTrainDirection(1);           //  update direction COUNTERCLOCKWISE
                        calculOutValue(def_01[t_last_block], 0, blockList[t_block_line0].getTrainDirection(), blockList[t_block_line0].getBusy(), 0);                       
                        delay(t_delay);
                        
                        if (i == num_elements-1)
                        {
                            erase_first_route();
                        }
                        erase_turnout_passed();
                        t_last_block = i;                       
                        break;
                }
            }
            else
            {
                //----------------------------- train is on turnouts ---------------------------                                         
                for (byte j = 0; j<50; j++)
                {
                    if ((def_01[i+1] == possible_route[1][j]) && (possible_route[3][j] == 1))
                    {                        
                        pin_route_j1 = j;
                        pin_route[j] = true;
                        R_Route::RouteChoice();   //  first button
                        break;
                    }
                }
                for (byte j = 0; j<50; j++)
                {
                    if ((def_01[i-1] == possible_route[1][j]) && (possible_route[3][j] == 0))
                    {                        
                        pin_route_j2 = j;
                        pin_route[j] = true;
                        R_Route::RouteChoice();   //  secound button
                        break;
                    }
                }
                drawing_routes();
            }
        }
    }    
}
//------------------------------- calculOutValue() -----------------------------------------------
void calculOutValue( byte aElement, byte aPosition, boolean aDirection, boolean aBusy, byte aSignal)
{
    int x1, x2, x3;
    int outValue = 0;
    
    //  ┌─┬─┬─┬─┬─┬─┬─┬─┐┌─┬─┐┌─┐┌─┐┌─┬─┐┌─┬─┐
    //    15 14  13 12  11 10  9  8    7  6     5    4    3   2    1  0
    //  └─┴─┴─┴─┴─┴─┴─┴─┘└─┴─┘└─┘└─┘└─┴─┘└─┴─┘
    //  15-8 : element    (# block or # turnout)
    //   7-6 : position   (straight or diverging)
    //     5 : direction  (clockwise or conterclockwise)
    //     4 : busy       (yes or no)
    //   3-2 : signal     (1 = stop (red), 2 = slow (orange), 3 = go (green))
    
    x1 = aBusy;                   //  busy in 0
    x1 = x1<<2;                   //  busy in 2
    x1 = x1 + aSignal;            //  busy in 2, signal in 1-0
    x1 = x1<<2;                   //  busy in 4, signal in 3-2
    x2 = aDirection;              //  direction in 0
    x2 = x2<<5;                   //  direction in 5 
    x2 = x2 + x1;                 //  direction in 5, busy in 4, signal in 3-2
    x3 = aPosition;               //  position in 1-0
    x3 = x3<<6;                   //  position in 7-6
    x3 = x3 + x2;                 //  position in 7-6, direction in 5, busy in 4, signal in 3-2
    outValue = aElement;          //  element in 8-0
    outValue = outValue<<8;       //  element in 15-8
    outValue = outValue + x3;     //  element in 15-8, position in 7-6, direction in 5, busy in 4, signal in 3-2
    
    //  In the example :
    //  OutValue = 2^8 *(114) + 2^6 *(1) + 2^5 *(1) + 2^4 *(1) + 2^2 *(3) = 29308

    Serial.println(outValue); // send 16 bits to Processing
/*
    Serial.println("Element Position Direction Busy Signal");
    Serial.print(aElement);
    Serial.print("        ");
    Serial.print(aPosition);
    Serial.print("          ");
    Serial.print(aDirection);
    Serial.print("      ");
    Serial.print(aBusy);
    Serial.print("   ");
    Serial.println(aSignal);
*/
}
//-------------------------- erase_first_route() ---------------------------------
void erase_first_route()
{
    pin_route[pin_route_j1] = false;
    pin_route[pin_route_j2] = false;
    for (byte k = 0; k<MAX_TURNOUT_ROUTE; k++)
    {                   
        if (w_routeList[pin_route_j1].getW_Turnout(k) != 0)
        {
            t_turnout_line = w_routeList[pin_route_j1].getW_Turnout(k)-1;                                 //  turnout line

            calculOutValue(w_routeList[pin_route_j1].getW_Turnout(k), w_routeList[pin_route_j1].getW_TurnoutPath(k), train_dir, FREE, 0);
            w_routeList[pin_route_j1].setW_StartBlock(0);
            w_routeList[pin_route_j1].setW_Turnout(k,0);
            w_routeList[pin_route_j1].setW_TurnoutPath(k,STRAIGHT);
            w_routeList[pin_route_j1].setW_Last(0);
            w_routeList[pin_route_j1].setW_EndBlock(0);
            w_routeList[pin_route_j1].setW_RouteStatus(FREE);
        }
        else
        {
            delay(t_delay);
            turnout_passed = false;
            break;
        }
    }
}
//-------------------- turnout_passed_erase() ----------------------------------------
void erase_turnout_passed()
{
    if (turnout_passed)                                     //  updte previous route free 
    {
        pin_route[pin_route_j1] = false;
        pin_route[pin_route_j2] = false;
        for (byte k = 0; k<MAX_TURNOUT_ROUTE; k++)
        {                   
            if (w_routeList[pin_route_j1].getW_Turnout(k) != 0)
            {
                t_turnout_line = w_routeList[pin_route_j1].getW_Turnout(k)-1;                                 //  turnout line
    
                calculOutValue(w_routeList[pin_route_j1].getW_Turnout(k), w_routeList[pin_route_j1].getW_TurnoutPath(k), train_dir, FREE, 0);
                w_routeList[pin_route_j1].setW_StartBlock(0);
                w_routeList[pin_route_j1].setW_Turnout(k,0);
                w_routeList[pin_route_j1].setW_TurnoutPath(k,STRAIGHT);
                w_routeList[pin_route_j1].setW_Last(0);
                w_routeList[pin_route_j1].setW_EndBlock(0);
                w_routeList[pin_route_j1].setW_RouteStatus(FREE);
            }
            else
            {
                delay(t_delay);
                break;
            }
        }
    }                                            
    turnout_passed = false;                        
}
//------------------------------ drawing routes -------------------------------------                
void drawing_routes()
{
    for (byte k = 0; k<MAX_TURNOUT_ROUTE; k++)
    {                   
        if (w_routeList[pin_route_j1].getW_Turnout(k) != 0)
        {
            t_turnout_line = w_routeList[pin_route_j1].getW_Turnout(k)-1;                                 //  turnout line
            turnoutList[t_turnout_line].setTurnoutPath(w_routeList[pin_route_j1].getW_TurnoutPath(k));    //  update turnout path
            turnoutList[t_turnout_line].setTrainDirection(train_dir);                                     //  update turnout train direction
    
            calculOutValue(w_routeList[pin_route_j1].getW_Turnout(k), w_routeList[pin_route_j1].getW_TurnoutPath(k), train_dir, BUSY, 3);
        }
        else
        {
            delay(t_delay);
            turnout_passed = true;
            break;
        }
    }
}

