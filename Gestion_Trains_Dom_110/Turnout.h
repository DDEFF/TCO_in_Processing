#ifndef TURNOUT_H
#define TURNOUT_H
#include <Arduino.h>

#include "Define.h"

class Turnout
{
    public :
        // ---------------------- Default constructor ---------------------------
        Turnout()
       {
           this->native_side                  = 0;
           this->current_turnout              = 0;
           this->straight_path                = 0;
           this->diverging_path               = 0;
           this->turnout_level_straight       = 0;
           this->turnout_level_diverging      = 0;
           this->turnout_max_speed            = 0;
           this->turnout_path                 = STRAIGHT;
           this->turnout_status               = FREE;
           this->train_direction              = CLOCKWISE;
       };
        
        // --------------------- Overload constructors -----------------------
        Turnout(
        byte native_side,
        byte current_turnout,
        byte straight_path,
        byte diverging_path,
        byte turnout_level_straight,
        byte turnout_level_diverging,
        byte turnout_max_speed,
        boolean turnout_path                = STRAIGHT,
        byte turnout_state                  = FREE,
        boolean train_direction             = CLOCKWISE);
        
        // ******************** Accessor functions ***************************
    
        inline byte    getNativeSide()            const { return this->native_side; }      
        inline byte    getCurrentTurnout()        const { return this->current_turnout; }      
        inline byte    getStraightPath()          const { return this->straight_path; }
        inline byte    getDivergingPath()         const { return this->diverging_path; }
        inline byte    getTurnoutLevelStraight()  const { return this->turnout_level_straight; }
        inline byte    getTurnoutLevelDiverging() const { return this->turnout_level_diverging; }   
        inline byte    getTurnoutMaxSpeed()       const { return this->turnout_max_speed; }
        inline boolean getTurnoutPath()           const { return this->turnout_path; }            
        inline byte    getTurnoutStatus()         const { return this->turnout_status; }      
        inline boolean getTrainDirection()        const { return this->train_direction; }
          
        // ****************** Mutator functions *******************************
    
        inline void setNativeSide            (byte    aNative_side)             { this->native_side             = aNative_side; }      
        inline void setCurrentTurnout        (byte    aCurrent_turnout)         { this->current_turnout         = aCurrent_turnout; }      
        inline void setStraightPath          (byte    aStraight_path)           { this->straight_path           = aStraight_path; }      
        inline void setDivergingPath         (byte    aDiverging_path)          { this->diverging_path          = aDiverging_path; }
        inline void setTurnoutLevelStraight  (byte    aTurnout_level_straight)  { this->turnout_level_straight  = aTurnout_level_straight; }
        inline void setTurnoutLevelDiverging (byte    aTurnout_level_diverging) { this->turnout_level_diverging = aTurnout_level_diverging; }     
        inline void setTurnoutMaxSpeed       (byte    aTurnout_max_speed)       { this->turnout_max_speed       = aTurnout_max_speed; }
        inline void setTurnoutPath           (boolean aTurnout_path)            { this->turnout_path            = aTurnout_path; }      
        inline void setTurnoutStatus         (byte    aTurnout_status)          { this->turnout_status          = aTurnout_status; }      
        inline void setTrainDirection        (boolean aTrain_direction)         { this->train_direction         = aTrain_direction; }
        
        //********************** Other functions **************************************  
        void PrintTurnout();
            // print fixed values in turnout
          
        byte TurnoutSearchFromBlock (byte aBlock, byte aTurnout);
            // search for a turnout from a block and the first turnout
        
    private :
    
        // Members variables
        
        // Defined once and for all :
        
          byte native_side;                    // # previous element on the native side
          byte current_turnout;                // # of current turnout
          byte straight_path;                  // # of next element after straight path
          byte diverging_path;                 // # of next element after diverging path
          byte turnout_level_straight;         // level of the turnout to the straight path in the station map
          byte turnout_level_diverging;        // level of the turnout to the diverging path in the station map
          byte turnout_max_speed;              // max speed on diverging path : 255, 160, 60, 30 (km/h)
        
        // Depending of trains and turnouts positions :
        
          boolean turnout_path;                // turnout path    : STRAIGHT (false) or DIVERGING (true)
          byte    turnout_status;              // turnout status  : FREE (0), RESERVED (1), BUSY (2)
          boolean train_direction;             // train direction : CLOCKWISE (false) or CONTERCLOCKWISE (true)
    
};

#endif

