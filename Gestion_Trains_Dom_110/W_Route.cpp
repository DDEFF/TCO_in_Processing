#include "W_Route.h"
extern W_Route w_routeList[];

#include <Arduino.h>

#include "Block.h"
extern Block blockList[];

#include "Turnout.h"
extern Turnout turnoutList[];

#include "R_Route.h"
extern R_Route r_routeList[];

#include "Variables.h"

//-------------------------------------
W_Route::W_Route(byte aW_Start_block, byte aW_Turnout[MAX_TURNOUT_ROUTE], boolean aW_Turnout_path[MAX_TURNOUT_ROUTE], byte aW_Last, byte aW_End_block , byte aW_Route_status, byte aW_Line, byte aW_First)
{
    this->w_start_block          = aW_Start_block;  
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        this->w_turnout[i]         = aW_Turnout[i];
        this->w_turnout_path[i]    = aW_Turnout_path[i];
    }  
    this->w_last                 = aW_Last;
    this->w_end_block            = aW_End_block;
    this->w_route_status         = aW_Route_status;
    this->w_line                 = aW_Line;
    this->w_first                = aW_First;
  
}

//**************************** Other functions ******************************************

//------------------------------ ResetW_Route() -------------------------------------

void W_Route::ResetW_Route()
{
    this->w_start_block             = 0;
    
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        this->w_turnout[i]      = 0;
        this->w_turnout_path[i] = STRAIGHT;
    }
    this->w_last                    = 0;
    this->w_end_block               = 0;
    this->w_route_status            = FREE;
}

//------------------------- PrintW_Route() --------------------------

void W_Route::PrintW_Route()
{
  //  if (w_start_block > 0)
  //  {
      Serial.print(this->w_start_block);
      Serial.print(" ");    
      for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
      {
          if (this->w_turnout[i] <10)
            Serial.print(" ");
          Serial.print(this->w_turnout[i]);
          Serial.print("->");
          Serial.print(this->w_turnout_path[i]);
          Serial.print(" ");
      }
      Serial.print(this->w_last);
      Serial.print(" ");
      Serial.print(this->w_end_block);
      Serial.print(" ");
      Serial.print(this->w_route_status);
      Serial.print(" ");
      Serial.print(this->w_line);
      Serial.print(" ");
      Serial.print(this->w_first);
      
  //  }
}

//------------------------------- W_LinesFirst() ------------------------------------------------- 

void W_Route::W_LineFirst()
{
    byte j = -1;
    for (byte i= 0;i < MAX_BLOCK; i++)
    {
        if ((blockList[i].getPreviousElement() < 101) && (blockList[i].getPreviousElement() != 0))
        {
             j = j+1;
             w_routeList[j].setW_Line(blockList[i].getCurrentBlock());
             w_routeList[j].setW_First(blockList[i].getPreviousElement());
             //------------------------------- possible_routes array creation ------------------------
             possible_route[0][j] = j;
             possible_route[1][j] = w_routeList[j].getW_Line();
             possible_route[2][j] = w_routeList[j].getW_First();
             possible_route[3][j] = 1;                          //  conter clockwise
        }
        if ((blockList[i].getNextElement() < 101) && (blockList[i].getNextElement() != 0))
        {
             j = j+1;
             w_routeList[j].setW_Line(blockList[i].getCurrentBlock());
             w_routeList[j].setW_First(blockList[i].getNextElement());
             //------------------------------- possible_routes array creation ------------------------
             possible_route[0][j] = j;
             possible_route[1][j] = w_routeList[j].getW_Line();
             possible_route[2][j] = w_routeList[j].getW_First();
             possible_route[3][j] = 0;                          //  clockwise
        }
    }
}

//--------------------------- WithChoice ----------------------------

void W_Route::WithChoice(byte aTurnout_level_straight, byte aTurnout_level_diverging, byte aFinal_level)
{
    if (aTurnout_level_straight == aFinal_level)
    {
        current_turnout_path = STRAIGHT;
        current_element = turnoutList[turnout_line].getStraightPath();
    }
    else
    {
        if (aTurnout_level_diverging == aFinal_level)
        {
            current_turnout_path = DIVERGING;
            current_element = turnoutList[turnout_line].getDivergingPath();
        }
        else
        {
            if (aFinal_level > aTurnout_level_straight)
            {
                if (aTurnout_level_straight > aTurnout_level_diverging)
                {
                    current_turnout_path = STRAIGHT;
                    current_element = turnoutList[turnout_line].getStraightPath();
                }
                else
                {
                    current_turnout_path = DIVERGING;
                    current_element = turnoutList[turnout_line].getDivergingPath();
                }        
            }
            else
            {
                if (aTurnout_level_straight > aTurnout_level_diverging)
                {
                    current_turnout_path = DIVERGING;
                    current_element = turnoutList[turnout_line].getDivergingPath();
                }
                else
                {
                    current_turnout_path = STRAIGHT;
                    current_element = turnoutList[turnout_line].getStraightPath();
                }                
            }
        }
    }
}

//--------------------------- W_AddTurnout0 ---------------------------  

void W_Route::W_AddTurnout0(byte aCurrent_element)
{
    for (byte i = 0; i < MAX_TURNOUT; i++)
    {
        turnout_line = i;
        turnoutList[turnout_line].TurnoutSearchFromBlock(aCurrent_element, first_turnout);
        if (found_turnout > 0)  // turnout is found
        {
            if (found_turnout > 100)  //  no choice for the turnout
            {
                found_turnout = found_turnout-100;
                w_routeList[w_route_line].setW_Turnout(0,found_turnout);
                if (turnoutList[turnout_line].getStraightPath() == aCurrent_element)
                {
                    w_routeList[w_route_line].setW_TurnoutPath(0,STRAIGHT);
                }
                else
                {
                    w_routeList[w_route_line].setW_TurnoutPath(0,DIVERGING);
                }
                current_element = turnoutList[turnout_line].getNativeSide();
                w_routeList[w_route_line].setW_Last(found_turnout);
                break;
            }
            else  //  the turnout offers a choice
            {
                w_routeList[w_route_line].setW_Turnout(0,found_turnout);
                w_routeList[w_route_line].setW_Last(found_turnout);
                WithChoice(turnoutList[turnout_line].getTurnoutLevelStraight(), turnoutList[turnout_line].getTurnoutLevelDiverging(), final_level);
                w_routeList[w_route_line].setW_TurnoutPath(0,current_turnout_path);
                break;        
            }
        }
    }
}

//--------------------------- W_AddTurnoutX ---------------------------  

void W_Route::W_AddTurnoutX(byte aCurrent_element, byte aPrevious_element)
{  
    turnout_line = aCurrent_element-1;
        
    if ((aPrevious_element == turnoutList[turnout_line].getStraightPath()) || (aPrevious_element == turnoutList[turnout_line].getDivergingPath()))  //  no choice for the turnout
    {
        if (turnoutList[turnout_line].getStraightPath() == aPrevious_element)
        {
            current_turnout_path = STRAIGHT;
        }
        else
        {
            current_turnout_path = DIVERGING;
        }
        current_element = turnoutList[turnout_line].getNativeSide();
        
    }
    else  //  the turnout offers a choice
    {
        WithChoice(turnoutList[turnout_line].getTurnoutLevelStraight(), turnoutList[turnout_line].getTurnoutLevelDiverging(), final_level);    
    }
}

//------------------------- W_RouteFilling -----------------------------------

void W_Route::W_RouteFilling()
{
    this->w_start_block = origine_block;                                                                                                                        
    this->w_end_block   = extremity_block;
  
    final_level = blockList[extremity_block-101].getBlockLevel();
    
    W_AddTurnout0(origine_block);
    
    for (byte i = 1; i < MAX_TURNOUT_ROUTE; i++)
    {
        if (current_element < 101)  //  Processig and continue because it's not a block
        {
            this->w_turnout[i] = current_element;
            this->w_last = this->w_turnout[i];
            W_AddTurnoutX(current_element,this->w_turnout[i-1]);
            this->w_turnout_path[i] = current_turnout_path;
        }
        else  //  Stop because it's a block
        {
            break;
        }
     } 
  
    if (current_element != extremity_block)  //  The wanted route is impossible
    {
      if (extremity_block != 0)
      {
          Serial.println();
          Serial.print("The wanted route is impossible : ");
          Serial.print(origine_block);
          Serial.print(" - ");
          Serial.println(extremity_block);
      }
      else
      {
          Serial.println();
          Serial.print("All routes beginning with ");
          Serial.print(origine_block);
          Serial.println(" are cancelled");
      }
      ResetW_Route();
  
    }
}

//--------------------------------W_TurnoutStatus -------------------------------------------------

void W_Route::W_TurnoutStatus(byte aCurrent_turnout)
{
    switch (turnoutList[aCurrent_turnout-1].getTurnoutStatus())
    {
      case FREE :
          this->w_route_status = FREE;    
          break;
      case RESERVED :
          this->w_route_status = CONFLICTING;
          break;
      case BUSY :
          this->w_route_status = CONFLICTING;
          break;
      default :
          Serial.println();
          Serial.print("Bad status of ");
          Serial.print(aCurrent_turnout);
    }
}

//-------------------------------- W_RouteStatusFree() -------------------------------------------------

void W_Route::W_RouteStatusFree()
{  
    for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
    {
        if ((this->w_route_status == FREE) && (this->w_turnout[i] != 0))
        {
            W_TurnoutStatus(this->w_turnout[i]);    
        }
        else
        {
            break;
        }
    }
}

//------------------------------- W_RouteStatus() ------------------------------------------------- 

void W_Route::W_RouteStatus()
{
    if ((this->w_route_status == FREE) && (this->w_start_block !=0))
    {
      for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
      {
          if ((this->w_turnout[i] != 0) && (turnoutList[this->w_turnout[i]-1].getTurnoutStatus() == FREE))
          {
              this->w_route_status = RESERVED;
          }
          else
          {
              if (this->w_turnout[i] != 0)
              {
                  this->w_route_status = FREE;
                  break;
              }       
          }
        }
        if (this->w_route_status == RESERVED)
        {
            for (byte i = 0; i < MAX_TURNOUT_ROUTE; i++)
            {
                if ((this->w_turnout[i] != 0) && (turnoutList[this->w_turnout[i]-1].getTurnoutStatus() == FREE))
                {
                    turnoutList[this->w_turnout[i]-1].setTurnoutStatus(RESERVED);
                }
            }
        }
    }
}
