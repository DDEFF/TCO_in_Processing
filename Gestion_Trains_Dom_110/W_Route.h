#ifndef W_ROUTE_H 
#define W_ROUTE_H
#include <Arduino.h>

#include "Define.h"

class W_Route    // W_Route for "Wanted Route"
{
    public :
        // ---------------------- Default constructor ---------------------------
        W_Route()
        {
            ResetW_Route();
        };
        
        // --------------------- Overload constructors -----------------------
        W_Route(byte start_block, byte turnout[MAX_TURNOUT_ROUTE], boolean turnout_path[MAX_TURNOUT_ROUTE], byte last, byte end_block, byte route_status, byte line, byte first);
        
        // ******************** Accessor functions ***************************
        
        inline byte    getW_StartBlock()              const { return this->w_start_block; }
        inline byte    getW_Turnout    (byte aIndex1) const { return this->w_turnout     [aIndex1]; }
        inline boolean getW_TurnoutPath(byte aIndex2) const { return this->w_turnout_path[aIndex2]; }
        inline byte    getW_Last()                    const { return this->w_last; }
        inline byte    getW_EndBlock()                const { return this->w_end_block; }
        inline byte    getW_RouteStatus()             const { return this->w_route_status; }
        inline byte    getW_Line()                    const { return this->w_line; }
        inline byte    getW_First()                   const { return this->w_first; }
                
        // ****************** Mutator functions *******************************
        
        inline void setW_StartBlock   (byte aW_Start_block)             { this->w_start_block           = aW_Start_block; }
        inline void setW_Turnout      (byte aIndex1, byte    aValue1)   { this->w_turnout     [aIndex1] = aValue1; }
        inline void setW_TurnoutPath  (byte aIndex2, boolean aValue2)   { this->w_turnout_path[aIndex2] = aValue2; }
        inline void setW_Last         (byte aW_Last)                    { this->w_last                  = aW_Last; }
        inline void setW_EndBlock     (byte aW_End_block)               { this->w_end_block             = aW_End_block; }
        inline void setW_RouteStatus  (byte aW_Route_status)            { this->w_route_status          = aW_Route_status; }
        inline void setW_Line         (byte aW_Line)                    { this->w_line                  = aW_Line; }
        inline void setW_First        (byte aW_First)                   { this->w_first                 = aW_First; }
        
        //********************** Other functions **************************************  
        void PrintW_Route();
            // print wanted routes
        
        void ResetW_Route();
            // reset all values
                
        static void W_LineFirst();
            // set line and first in w_route
          
        void WithChoice(byte aTurnout_level_straight, byte aTurnout_level_diverging, byte aFinal_level);
            // There is a choice for the path
          
        void W_AddTurnout0(byte aCurrent_element);
            // add turnout[0]
          
        void W_AddTurnoutX(byte aCurrent_element, byte aPrevious_element);
            // add turnout_X to the turnout X-1
          
        void W_RouteFilling();
            // Fill the w_route
          
        void W_TurnoutStatus(byte aCurrent_turnout);
            // Calculation of the turnout status
    
        void W_RouteStatusFree();
            // Search for route_status = free
          
        void W_RouteStatus();
            // Calculation of the status of each route of the w_route
                    
    private :
    
        // Members variables
        
        // Depending of turnouts positions :
      
        byte    w_start_block;                     // # of start block (wanted train direction)
        byte    w_turnout     [MAX_TURNOUT_ROUTE]; // Here : 20 turnouts from 0 to 19
        boolean w_turnout_path[MAX_TURNOUT_ROUTE]; // path of each turnout : STRAIGHT (false) or DIVERGING (true)      
        byte    w_last;                            // last turnout for the w_route
        byte    w_end_block;                       // # of end block (wanted train direction)
        byte    w_route_status;                    // FREE (0), RESERVED (1), BUSY (2), CONFLICTING (3)
        byte    w_line;                            // line of possible starting block
        byte    w_first;                           // first turnout for the w_route
        
};

#endif

