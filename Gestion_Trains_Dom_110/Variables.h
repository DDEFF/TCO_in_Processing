extern byte block_line;
extern byte turnout_line;
extern byte route_line;
extern byte r_route_line;
extern byte w_route_line;
extern byte total_train;
extern byte t_block_line;
extern byte t_turnout_line;

extern byte current_element;
extern byte current_signal;
extern byte found_turnout;
extern boolean found_train;

extern byte origine_block;
extern byte extremity_block;
extern byte first_turnout;

extern byte final_level;
extern byte current_element;
extern byte current_priority;
extern byte total_pin_route;
extern byte total_train;

extern byte new_route_init[];

extern boolean current_turnout_path;
extern boolean route_direction;
extern boolean pin_route[];

extern unsigned long last_route_date;
extern unsigned long dateCourante;
extern unsigned long intervalle;
extern unsigned long dateDernierChangement;

extern boolean aClw_stop_zone_status;
extern boolean aPlain_track_status;
extern boolean aCclw_stop_zone_status;

extern boolean test_000;
extern boolean test_001;
extern boolean test_010;
extern boolean test_011;
extern boolean test_100;
extern boolean test_101;
extern boolean test_110;
extern boolean test_111;
extern boolean memo_000;
extern boolean memo_001;
extern boolean memo_010;
extern boolean memo_011;
extern boolean memo_100;
extern boolean memo_101;
extern boolean memo_110;
extern boolean memo_111;
extern boolean block_direction;

extern byte pass_direction;
extern byte possible_route[4][50];
extern byte pin_route_j1;
extern byte pin_route_j2;
extern byte t_block_line0;
extern byte t_block_line1;
extern byte t_last_block;

extern boolean train_dir;
extern boolean turnout_passed;
extern int t_delay;

