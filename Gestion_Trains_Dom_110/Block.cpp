#include "Block.h"
extern Block blockList[];

#include "Variables.h"
#include <Arduino.h>

#include "W_Route.h"
extern W_Route w_routeList[];

#include "Route.h"
extern Route routeList[];

Block::Block(byte aPrevious_element, byte aCurrent_block, byte aNext_element, byte aBlock_level, byte aBlock_length, byte aStop_length,
byte aConter_stop_length, byte aPrevious_block, byte aNext_block, byte aClock_signal, byte aConter_signal, boolean aBusy, boolean aTrain_direction, 
boolean aClw_stop_zone_status, boolean aPlain_track_status, boolean aCclw_stop_zone_status,
boolean aClw_stop_zone_status_0, boolean aPlain_track_status_0, boolean aCclw_stop_zone_status_0)
{
    this->previous_element                  = aPrevious_element;
    this->current_block                     = aCurrent_block;
    this->next_element                      = aNext_element;
    this->block_level                       = aBlock_level;
    this->block_length                      = aBlock_length;
    this->stop_length                       = aStop_length;
    this->conter_stop_length                = aConter_stop_length;
    this->previous_block                    = aPrevious_block;
    this->next_block                        = aNext_block;
    this->clock_signal                      = aClock_signal;
    this->conter_signal                     = aConter_signal;
    this->busy                              = aBusy;
    this->train_direction                   = aTrain_direction;
    this->clw_stop_zone_status              = aClw_stop_zone_status;
    this->plain_track_status                = aPlain_track_status;
    this->cclw_stop_zone_status             = aCclw_stop_zone_status;
    this->clw_stop_zone_status_0            = aClw_stop_zone_status_0;
    this->plain_track_status_0              = aPlain_track_status_0;
    this->cclw_stop_zone_status_0           = aCclw_stop_zone_status_0;
}

//*************************** Other functions *********************************

//------------------------- PrintBlock() --------------------------
void Block::PrintBlock()
{
    Serial.println();  
    Serial.print  ("Previous element, Current block, Next element,    Previous block,  Next block, Busy,      ConterSignal,      ClockSignal,  Direction");
    Serial.println();
    Serial.print  ("\t");
    Serial.print  (previous_element);
    Serial.print  ("          ");
    Serial.print  ("\t");
    Serial.print  (current_block);
    Serial.print  ("     ");
    Serial.print  ("\t");
    Serial.print  (next_element);
    Serial.print  ("       ");
    Serial.print  ("\t");
    Serial.print  (previous_block);
    Serial.print  ("       ");
    Serial.print  ("\t");
    Serial.print  (next_block);
    Serial.print  ("  ");
    Serial.print  ("\t");
    Serial.print  (busy);
    Serial.print  ("          ");
    Serial.print  ("\t");
    Serial.print  (conter_signal);
    Serial.print  ("          ");
    Serial.print  ("\t");
    Serial.print  (clock_signal);
    Serial.print  ("\t");
    Serial.print  (train_direction);
    Serial.print  ("\t");
    Serial.print  ("          ");
    Serial.print  (clw_stop_zone_status);
    Serial.print  ("\t");
    Serial.print  (plain_track_status);
    Serial.print  ("\t");
    Serial.print  (cclw_stop_zone_status);
    Serial.print  ("\t");
    Serial.print  (clw_stop_zone_status_0);
    Serial.print  ("\t");
    Serial.print  (plain_track_status_0);
    Serial.print  ("\t");
    Serial.print  (cclw_stop_zone_status_0);
}

//------------------------ NextBlockRoute() ----------------------------
// This is the case of a turnout

void Block::NextBlockRoute(byte aCurrent_element, boolean aDirection)
{
    for (byte i = 0; i < MAX_ROUTE; i++)
    {
        for (byte j = 0; j < MAX_TURNOUT_ROUTE; j++)
        {
            if (routeList[i].getTurnout(j) == aCurrent_element)
            {
                if ((aDirection == CLOCKWISE) && (current_block == routeList[i].getStartBlock()))
                {
                    next_block = routeList[i].getEndBlock();
                }
                if ((aDirection == CLOCKWISE) && (current_block == routeList[i].getEndBlock()))
                {
                    previous_block = routeList[i].getStartBlock();
                }
                if ((aDirection == CONTERCLOCKWISE) && (current_block == routeList[i].getStartBlock()))
                {
                    next_block = routeList[i].getEndBlock();
                }
                if ((aDirection == CONTERCLOCKWISE) && (current_block == routeList[i].getEndBlock()))
                {
                    previous_block = routeList[i].getStartBlock();
                }
            }
        }
    }
}

//------------------------ NextBlock() ----------------------------
// There is a difference between next_element and next_block : next_element could be a turnout

void Block::NextBlock()
{
    if (next_element > 100)
    {
        next_block = next_element;
    }
    else
    {
        NextBlockRoute(next_element,CLOCKWISE);
    }
    if (previous_element > 100)
    {
        previous_block = previous_element;
    }
    else
    {
        NextBlockRoute(previous_element,CONTERCLOCKWISE);
    }
}

//------------------------------------------------- SignalsFilling() ----------------------------------------------
//  This is a "static" void because we work on the entire blockList[]

void Block::SignalsFilling()
{
    //----------------------------- Filling for VL, C and S -----------------------------------------------------------------------------
  
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
        //------- CLOCKWISE ---------------  
        blockList[i].setClockSignal(VL);
        if (blockList[blockList[i].getNextBlock()-101].getBusy() == BUSY)
          blockList[i].setClockSignal(S);      
    
        if (blockList[i].getNextBlock() == 0)
          blockList[i].setClockSignal(C);
    
        //------ CONTERCLOCKWISE ----------------
        blockList[i].setConterSignal(VL);
        if (blockList[blockList[i].getPreviousBlock()-101].getBusy() == BUSY)
          blockList[i].setConterSignal(S);      
    
        if (blockList[i].getPreviousBlock() == 0)
          blockList[i].setConterSignal(C);
    }
  
    //----------------------------- Filling for A -----------------------------------------------------------------------------
  
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
        //------- CLOCKWISE ---------------      
        if ((blockList[blockList[i].getNextBlock()-101].getClockSignal() == S) || (blockList[blockList[i].getNextBlock()-101].getClockSignal() == C))
          if ((blockList[i].getClockSignal() != C) && (blockList[i].getClockSignal() != S))
            blockList[i].setClockSignal(A);      
    
        //------ CONTERCLOCKWISE ----------------
        if ((blockList[blockList[i].getPreviousBlock()-101].getConterSignal() == S) || (blockList[blockList[i].getPreviousBlock()-101].getConterSignal() == C))
          if ((blockList[i].getConterSignal() != C) && (blockList[i].getConterSignal() != S))
            blockList[i].setConterSignal(A);      
    }
  
    //----------------------------- Filling for R and RR ----------------------------------------------------------------------
  
    for (byte i = 0; i < MAX_BLOCK; i++)
    {
      //------- Clockwise ---------------      
        for (byte j = 0; j < MAX_ROUTE; j++)
        {
            if (blockList[i].getCurrentBlock() == routeList[j].getStartBlock())
            {
                switch (routeList[j].getRouteMaxSpeed())
                {
                    case 30 :
                        if ((blockList[i].getClockSignal() != C) && (blockList[i].getClockSignal() != S))
                        {
                          blockList[i].setClockSignal(blockList[i].getClockSignal()+10);
                          if ((blockList[blockList[i].getPreviousBlock()-101].getClockSignal() != C) && (blockList[blockList[i].getPreviousBlock()-101].getClockSignal() != S))
                            blockList[blockList[i].getPreviousBlock()-101].setClockSignal(blockList[blockList[i].getPreviousBlock()-101].getClockSignal()+100);
                        }            
                        break;
                    case 60 :
                        if ((blockList[i].getClockSignal() != C) && (blockList[i].getClockSignal() != S))
                        {
                          blockList[i].setClockSignal(blockList[i].getClockSignal()+20);
                          if ((blockList[blockList[i].getPreviousBlock()-101].getClockSignal() != C) && (blockList[blockList[i].getPreviousBlock()-101].getClockSignal() != S))
                            blockList[blockList[i].getPreviousBlock()-101].setClockSignal(blockList[blockList[i].getPreviousBlock()-101].getClockSignal()+200);
                        }            
                        break;
                    case 160 :
                        break;
                }
            }      
        //------ Conterclockwise ----------------
            if (blockList[i].getCurrentBlock() == routeList[j].getEndBlock())
            {
                switch (routeList[j].getRouteMaxSpeed())
                {
                    case 30 :
                        if ((blockList[i].getConterSignal() != C) && (blockList[i].getConterSignal() != S))
                        {
                          blockList[i].setConterSignal(blockList[i].getConterSignal()+10);
                          if ((blockList[blockList[i].getNextBlock()-101].getConterSignal() != C) && (blockList[blockList[i].getNextBlock()-101].getConterSignal() != S))
                            blockList[blockList[i].getNextBlock()-101].setConterSignal(blockList[blockList[i].getNextBlock()-101].getConterSignal()+100);
                        }            
                        break;
                    case 60 :
                        if ((blockList[i].getConterSignal() != C) && (blockList[i].getConterSignal() != S))
                        {
                          blockList[i].setConterSignal(blockList[i].getConterSignal()+20);
                          if ((blockList[blockList[i].getNextBlock()-101].getConterSignal() != C) && (blockList[blockList[i].getNextBlock()-101].getConterSignal() != S))
                            blockList[blockList[i].getNextBlock()-101].setConterSignal(blockList[blockList[i].getNextBlock()-101].getConterSignal()+200);
                        }            
                        break;
                    case 160 :
                        break;
                }
            }      
        }
    }
}


