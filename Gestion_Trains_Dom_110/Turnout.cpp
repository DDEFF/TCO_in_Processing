#include "Turnout.h"

#include "Variables.h"
#include <Arduino.h>

Turnout::Turnout(byte aNative_side, byte aCurrent_turnout, byte aStraight_path, byte aDiverging_path, byte aTurnout_level_straight, byte aTurnout_level_diverging, byte aTurnout_max_speed, boolean aTurnout_path, byte aTurnoutStatus, boolean aTrain_direction)
{
    this->native_side             = aNative_side;
    this->current_turnout         = aCurrent_turnout;
    this->straight_path           = aStraight_path;
    this->diverging_path          = aDiverging_path;
    this->turnout_level_straight  = aTurnout_level_straight;
    this->turnout_level_diverging = aTurnout_level_diverging;
    this->turnout_max_speed       = aTurnout_max_speed;
    this->turnout_path            = aTurnout_path;
    this->turnout_status          = aTurnoutStatus;
    this->train_direction         = aTrain_direction;
}

//**************************** Other functions ******************************************

//------------------------- PrintTurnout --------------------------

void Turnout::PrintTurnout()
{
    Serial.println();  
    Serial.print  ("Native side, Current turnout, Straight path, Diverging path");
    Serial.println();
    Serial.print  ("       ");
    Serial.print  (native_side);
    Serial.print  ("              ");
    Serial.print  (current_turnout);
    Serial.print  ("             ");
    Serial.print  (straight_path);
    Serial.print  ("              ");
    Serial.print  (diverging_path);
}

//------------------------ TurnoutSearchFromBlock --------------------------------

byte Turnout::TurnoutSearchFromBlock(byte aBlock, byte aTurnout)
{
    found_turnout = 0;
    if ((aBlock == this->native_side) && (aTurnout == this->current_turnout))
    {
        found_turnout = this->current_turnout;
        return found_turnout;    
    }
    else
    {
        if (((aBlock == this->straight_path) || (aBlock == this->diverging_path)) && (aTurnout == this->current_turnout))
        {
            found_turnout = this->current_turnout+100;
            return found_turnout;
        }
    }
}

