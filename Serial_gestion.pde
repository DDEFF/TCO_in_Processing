//------------------ Serial gestion ----------------------
void serial_gestion()
{
    if (exemple)
    {        
        String inString = myPort.readStringUntil('\n');
        
        if (inString != null)
        {
            // spaces extract
            inString = trim(inString);
                
            //  ┌──┬──┬──┬──┬──┬──┬──┬──┐┌──┬──┐┌──┐┌──┐┌──┬──┐┌──┬──┐
            //  │15│14│13│12│11│10│ 9│ 8││ 7│ 6││ 5││ 4││ 3│ 2││ 1│ 0│
            //  └──┴──┴──┴──┴──┴──┴──┴──┘└──┴──┘└──┘└──┘└──┴──┘└──┴──┘
            //  15-8 : element    (# block or # turnout)
            //   7-6 : position   (straight or diverging)
            //     5 : direction  (clockwise or conterclockwise)
            //     4 : busy       (yes or no)
            //   3-2 : signal     (1 = stop (red), 2 = slow (orange), 3 = go (green))
        
            int inValue = int(inString);                                  //             example : 29308
            int x1, x2, x3, x4;
            
            extracted_element   = inValue>>8;                             //    29308/2^8      =     114
            x1                  = inValue - 256*extracted_element;        //  29308 - 2^8 *114 = 124
            extracted_position  = x1>>6;                                  //      124/2^6      =       1
            x2                  = x1      - 64*extracted_position;        //    124 - 2^6 *  1 =  60          
            extracted_direction = boolean(x2>>5);                         //       60/2^5      =       1
            x3                  = x2      - 32*int(extracted_direction);  //     60 - 2^5 *  1 =  28
            extracted_busy      = boolean(x3>>4);                         //       28/2^4      =       1
            x4                  = x3      - 16*int(extracted_busy);       //     28 - 2^4 *  1 =  12
            extracted_signal    = byte(x4>>2);                            //       12/2^2      =       3 
    
            if ((extracted_element >= 1) && (extracted_element <= 255))
            {
                println(inValue);
                RailIDList[extracted_element-1].position  = byte(extracted_position);
                RailIDList[extracted_element-1].direction = extracted_direction;
                RailIDList[extracted_element-1].busy      = extracted_busy;
                RailIDList[extracted_element-1].signal    = extracted_signal;
//                if ((extracted_element == 12) || (extracted_element == 13))
//                {
                  println("inValue = "+inValue+"  //  ID = "+(extracted_element)+"  //  position = "+RailIDList[extracted_element-1].position+"  //  direction = "+RailIDList[extracted_element-1].direction+"  //  busy = "+RailIDList[extracted_element-1].busy+"  //  signal = "+RailIDList[extracted_element-1].signal);
//                }
            }     
        }
    }
}