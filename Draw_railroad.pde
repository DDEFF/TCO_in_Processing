void draw_railroad()
{
    for (int x = 0; x<width; x = x+edge)
    {
        int i = floor(x/edge);
        for (int y = 0; y<height; y = y+edge)
        {
            int j = floor(y/edge);

            if ((cubeList[i][j].ID != 0) && (RailIDList[cubeList[i][j].ID-1].busy == true))
            {  //<>//
                switch (RailIDList[(cubeList[i][j].ID)-1].signal)
                {
                    case 0:
                        draw_red   = rail_forecolor_red;
                        draw_green = rail_forecolor_green;
                        draw_blue  = rail_forecolor_blue;
                        break;
                    case 1:
                        draw_red   = signal_stop_red;
                        draw_green = signal_stop_green;
                        draw_blue  = signal_stop_blue;                  
                        break;
                    case 2:
                        draw_red   = signal_slow_red;
                        draw_green = signal_slow_green;
                        draw_blue  = signal_slow_blue;                  
                        break;
                    case 3:
                        draw_red   = signal_go_red;
                        draw_green = signal_go_green;
                        draw_blue  = signal_go_blue;                  
                        break;
                }
                switch (cubeList[i][j].type)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                    case 8:
                        draw_cube (x, y, i, j, RailIDList[(cubeList[i][j].ID-1)].position, draw_red, draw_green, draw_blue);
                        break;
                        
                    case 4:  //-------------------------------------- double slip -----------------------------------------------------------
                        position_1 = RailIDList[(cubeList[i][j].ID)-2].position;
                        position_2 = RailIDList[(cubeList[i][j].ID)-1].position;
                        
                        position_1_2 = 10*int(position_1)+int(position_2);
                      
                        switch (cubeList[i][j].part[0])
                        {
                            case 1 :  //  1-5
                            case 2:
                            case 3:
                            case 4:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 1;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 2;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 3;
                                        break;
                                    case 11:
                                        cubeList[i][j].part[0] = 4;
                                        break;
                                }
                                break;
                            case 5 :  //  2-6
                            case 6:
                            case 7:
                            case 8:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 5;  
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 6;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 7;
                                        break;
                                    case 11:
                                        cubeList[i][j].part[0] = 8;
                                        break;
                                }
                                break;
                            case 9 :  //  3-7
                            case 10:
                            case 11:
                            case 12:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 9;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 10;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 11;
                                        break;
                                    case 11:
                                        cubeList[i][j].part[0] = 12;
                                        break;
                                }
                                break;
                            case 13 :  //  4-8
                            case 14:
                            case 15:
                            case 16:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 16;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 14;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 15;
                                        break;
                                    case 11:
                                        cubeList[i][j].part[0] = 13;
                                        break;
                                }
                                break;
                        }            
                        draw_cube (x, y, i, j, byte(0), draw_red, draw_green, draw_blue);
                        break;
                        
                    case 5:  //-------------------------------------- triple -----------------------------------------------------------
                        position_1 = RailIDList[(cubeList[i][j].ID)-2].position;
                        position_2 = RailIDList[(cubeList[i][j].ID)-1].position;
                        
                        position_1_2 = 10*int(position_1)+int(position_2);
                      
                        switch (cubeList[i][j].part[0])
                        {
                            case 1 :  //  1-5
                            case 2:
                            case 3:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 1;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 2;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 3;
                                        break;
                                }
                                break;
                            case 4 :  //  2-6
                            case 5:
                            case 6:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 4;  
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 5;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 6;
                                        break;
                                }
                                break;
                            case 7 :  //  3-7
                            case 8:
                            case 9:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 7;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 8;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 9;
                                        break;
                                }
                                break;
                            case 10 :  //  4-8
                            case 11:
                            case 12:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 10;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 11;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 12;
                                        break;
                                }
                                break;
                            case 13 :  //  5-1
                            case 14:
                            case 15:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 13;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 14;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 15;
                                        break;
                                }
                                break;
                            case 16 :  //  6-2
                            case 17:
                            case 18:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 16;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 17;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 18;
                                        break;
                                }
                                break;
                            case 19 :  //  7-3
                            case 20:
                            case 21:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 19;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 20;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 21;
                                        break;
                                }
                                break;
                            case 22 :  //  8-4
                            case 23:
                            case 24:
                                switch (position_1_2)
                                {
                                    case 0:
                                        cubeList[i][j].part[0] = 22;
                                        break;
                                    case 1:
                                        cubeList[i][j].part[0] = 23;
                                        break;
                                    case 10:
                                        cubeList[i][j].part[0] = 24;
                                        break;
                                }
                                break;
                        }            
                        draw_cube (x, y, i, j, byte(0), draw_red, draw_green, draw_blue);
                        break;
                }
            }
            else
            {
                draw_red   = rail_forecolor_red;
                draw_green = rail_forecolor_green;
                draw_blue  = rail_forecolor_blue;                
                draw_cube (x, y, i, j, byte(0), draw_red, draw_green, draw_blue);
            }
        }
    }  
}                