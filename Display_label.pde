//------------------------------------ display_label() ----------------------
void display_label()
{
    stroke(0, 0, 0);
    for (int i = 0; i<255; i++)
    {
        textFont(f,floor(edge/5));
        if (RailIDList[i].type != 4)
        {

            int x_label = edge*RailIDList[i].block_piece_x[0];
            int y_label = edge*RailIDList[i].block_piece_y[0];
            if (x_label != 0)
            {
                fill(255);
                rect(x_label+edge/8, y_label+edge/8, 6*edge/8, 6*edge/8);
                fill(0);
                textAlign(CENTER, CENTER);
                text(RailIDList[i].ID, x_label+edge/2, y_label+edge/2);
                cubeList[RailIDList[i].block_piece_x[0]][RailIDList[i].block_piece_y[0]].ID = RailIDList[i].ID;
                for (int j = 1; j<max_cube; j++)
                {
                    x_label = edge*RailIDList[i].block_piece_x[j];
                    y_label = edge*RailIDList[i].block_piece_y[j];
                    if (x_label != 0)
                    {
                        fill(255);
                        rect(x_label+edge/4, y_label+edge/4, edge/2, edge/2);
                        fill(0);
                        textAlign(CENTER, CENTER);
                        text(RailIDList[i].ID, x_label+edge/2, y_label+edge/2);
                        cubeList[RailIDList[i].block_piece_x[j]][RailIDList[i].block_piece_y[j]].ID = RailIDList[i].ID;
                    }
                }
            }
        }
        else
        {
            int x_label = edge*RailIDList[i].block_piece_x[0];
            int y_label = edge*RailIDList[i].block_piece_y[0];
            fill(255);
            rect(x_label+edge/4, y_label, edge/2, edge/2);
            fill(0);
            textAlign(CENTER, CENTER);
            text(RailIDList[i].ID, x_label+edge/2, y_label+edge/4);
            cubeList[RailIDList[i].block_piece_x[0]][RailIDList[i].block_piece_y[0]].ID = RailIDList[i].ID;
            
            i++;
            
            x_label = edge*RailIDList[i].block_piece_x[0];
            y_label = edge*RailIDList[i].block_piece_y[0];
            fill(255);
            rect(x_label+edge/4, y_label+edge/2, edge/2, edge/2);
            fill(0);
            textAlign(CENTER, CENTER);
            text(RailIDList[i].ID, x_label+edge/2, y_label+3*edge/4);
            cubeList[RailIDList[i].block_piece_x[0]][RailIDList[i].block_piece_y[0]].ID = RailIDList[i].ID;
        }
    }
    noStroke();
}