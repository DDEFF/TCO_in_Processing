int     edge;
float   thickness;
float   diag;

float   bx, by;

boolean move_one_locked = false;
boolean ctrl_locked     = false;
boolean copy_locked     = false;
boolean cut_locked      = false;
boolean shift_locked    = false;
boolean memory_locked   = false;
boolean shift_C_locked  = false;
boolean exemple         = false;
boolean grid            = false;
boolean block_signal    = false;
float   xOffset         = 0.0; 
float   yOffset         = 0.0;
float   corner_color;

int     i_cube, j_cube;
float   x_cube, y_cube;
int     i_serial, j_serial;

byte    cube_position;
int     max_column, max_line;
float   previous_x, previous_y;
int     x_ref, y_ref;
byte    type_a;
int     previous_type;
byte    order;
byte    max_cube         = 25;
boolean max_cube_reached = false;
boolean block_text       = false;
int     offset_x, offset_y;

int     extracted_element;
int     extracted_position;
boolean extracted_direction  = false;
boolean extracted_busy       = false;
byte    extracted_signal;
boolean find_new_ID;
int     count_ID;

float   draw_red, draw_green, draw_blue;

int     position_1, position_2, position_1_2;

int     draw_position;

//------------------- rail's backcolor ------------
float   rail_backcolor_red   = 200;
float   rail_backcolor_green = 200;
float   rail_backcolor_blue  = 200;
//------------------- rail's forecolor -------------
float   rail_forecolor_red   = 0;
float   rail_forecolor_green = 0;
float   rail_forecolor_blue  = 0;
//------------------- normal cube's backcolor -------------
//  RAL 1013 = 227, 217, 198
//  RAL 1014 = 221, 196, 154
//  RAL 1015 = 230, 210, 181
//  RAL 6019 = 185, 206, 172
//  RAL 6021 = 138, 153, 119
//  RAL 9001 = 233, 224, 210
//  RAL 9010 = 241, 236, 225
float   normal_cube_red      = 233;
float   normal_cube_green    = 224;
float   normal_cube_blue     = 210;
//------------------- selected cube's backcolor -------------
float   selected_cube_red    = 180;
float   selected_cube_green  = 180;
float   selected_cube_blue   = 180;
//------------------- signal_stop -----------------------------
//  RAL 3026 = 255, 42, 27 (red)
//  RAL 3024 = 255, 45, 33
float   signal_stop_red      = 255;
float   signal_stop_green    = 45;
float   signal_stop_blue     = 33;
//------------------- signal_slow -----------------------------
//  RAL 2010 = 208, 93, 40 (orange)
//  RAL 2008 = 237, 107, 33
float   signal_slow_red      = 237;
float   signal_slow_green    = 107;
float   signal_slow_blue     = 33;
//------------------- signal_go -----------------------------
//  RAL 6002 = 50, 89, 40  (green)
//  RAL 6037 = 0, 139, 41
float   signal_go_red        = 0;
float   signal_go_green      = 139;
float   signal_go_blue       = 41;

Cube[][]         cubeList         = new Cube[100][55];          //  Background
SelectedCube[][] SelectedCubeList = new SelectedCube[100][55];  //  Foreground
RailID[]         RailIDList       = new RailID[256];            //  RailID

Table   panel;
Table   railID;

int[]   previous_part  = new int[4];
int[]   block_piece_x  = new int[max_cube];
int[]   block_piece_y  = new int[max_cube];
int[]   list_ID        = new int[255];


import processing.serial.*;
Serial myPort;

import static javax.swing.JOptionPane.*; 
StringList ids = new StringList( new String[] {"0"});

PFont f;

//****************************** setup()****************************************
void setup()
{    
    colorMode(RGB);
    strokeCap(SQUARE);
    
    /********************************/
    /**                            **/
    /**/       size(1920,900);   /**/
    /**/       edge = 40;         /**/
    /**                            **/
    /********************************/
    
    bx        = width/2;
    by        = height/2;
    thickness = edge/20;
    diag      = thickness/sqrt(2);
    
    max_column = 1+floor(width/edge);
    max_line   = 1+floor(height/edge);

    //-------------------------------------- Class définitions -------------------------------------
    for (int i = 0; i<max_column; i++)
    {
        for (int j = 0; j<max_line; j++)
        {
            cubeList[i][j]         = new Cube        (i, j, byte(0), byte(0), byte(0), byte(0), byte(0), false, 0.0, 0.0, 0.0, false, false, 0);
            SelectedCubeList[i][j] = new SelectedCube(i, j, byte(0), byte(0), byte(0), byte(0), byte(0), 0, false);
            
            fill(normal_cube_red, normal_cube_green, normal_cube_blue);    //  Normal color for cube's backcolor 
            rect(i*edge, j*edge, edge, edge);
            reflection(i*edge, j*edge, edge);
        }
    }
    int x[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    for (int i = 0; i<256; i++)
    {
        RailIDList[i] = new RailID(i, byte(0), byte(0), false, false, byte(0), byte(0), x, x);
    }
    mousePressed();
    mouseReleased();

    //------------------------------- panel Table() create ----------------------------
    panel = new Table();
    
    panel.addColumn("x0");
    panel.addColumn("y0");
    panel.addColumn("type");
    panel.addColumn("part[0]");
    panel.addColumn("part[1]");
    panel.addColumn("part[2]");
    panel.addColumn("part[3]");
    panel.addColumn("ID");

    for (int k = 0; k<(max_column*max_line/5); k++)
    {
        TableRow newRow = panel.addRow();
        newRow.setInt("x0"            , cubeList[0][0].x0);
        newRow.setInt("y0"            , cubeList[0][0].y0);
        newRow.setInt("type"          , cubeList[0][0].type);
        newRow.setInt("part[0]"       , cubeList[0][0].part[0]);
        newRow.setInt("part[1]"       , cubeList[0][0].part[1]);
        newRow.setInt("part[2]"       , cubeList[0][0].part[2]);
        newRow.setInt("part[3]"       , cubeList[0][0].part[3]);
        newRow.setInt("ID"            , cubeList[0][0].ID);
    }

    //------------------------------- railID Table() create -----------------------------------
    railID = new Table();
    
    railID.addColumn("ID");
    railID.addColumn("type");
    railID.addColumn("position");
    railID.addColumn("direction");
    railID.addColumn("busy");
    railID.addColumn("signal");
    railID.addColumn("number_pieces");
    for (byte m = 0; m<max_cube; m++)
    {
        railID.addColumn("block_piece_x["+m+"]");
        railID.addColumn("block_piece_y["+m+"]");
    }
    
    for (int k = 0; k<256; k++)
    {
        TableRow newRow = railID.addRow();
        newRow.setInt("ID"            , RailIDList[k].ID);
        newRow.setInt("type"          , RailIDList[k].type);
        newRow.setInt("position"      , RailIDList[k].position);
        newRow.setInt("direction"     , 0);
        newRow.setInt("busy"          , 0);
        newRow.setInt("signal"        , 0);
        newRow.setInt("number_pieces" , RailIDList[k].number_pieces);
        for (byte m = 0; m<max_cube; m++)
        {
            newRow.setInt("block_piece_x["+m+"]" , RailIDList[k].block_piece_x[m]);
            newRow.setInt("block_piece_y["+m+"]" , RailIDList[k].block_piece_y[m]);
        }
    }
    
    //--------------------- Fonts for the texts ---------------------
    f = createFont("Arial",8,true); // Create Font

    //---------------------- Serial gestion -------------------
    
    String COMx, COMlist = "";

    try 
    {
        printArray(Serial.list());
        int i = Serial.list().length;
        if (i != 0)
        {
            if (i >= 2)
            {
                // need to check which port the inst uses -
                // for now we'll just let the user decide
                for (int j = 0; j < i;) 
                {
                    COMlist += char(j+'a') + " = " + Serial.list()[j];
                    if (++j < i) COMlist += ",  ";
                }
                COMx = showInputDialog("Which COM port is correct? (a,b,..):\n"+COMlist);
                if (COMx == null) exit();
                if (COMx.isEmpty()) exit();
                i = int(COMx.toLowerCase().charAt(0) - 'a') + 1;
            }
            String portName = Serial.list()[i-1];
            println(portName);
            myPort = new Serial(this, portName, 115200); // change baud rate to your liking
            myPort.bufferUntil('\n'); // buffer until CR/LF appears, but not required..
        }
        else 
        {
            showMessageDialog(frame,"Device is not connected to the PC");
            exit();
        }
    }

    catch (Exception e)
    { //Print the type of error
        showMessageDialog(frame,"COM port is not available (may\nbe in use by another program)");
        println("Error:", e);
        exit();
    }   
}
//********************************** draw() ************************************
void draw()
{
    if (!grid)
    {
        legend();
    }
    else
    {
        for (int i = 0; i<max_line; i++)
        {
            cubeList[0][i].part[0] = 0;          
            cubeList[0][i].part[1] = 0;          
        }
    }

    draw_railroad();
    cube_move();
    serial_gestion();
    //---------------------------- display labels ----------------------
    if (block_text)
    {
        display_label();
    }
}
//******************************* Others ****************************
//------------------------------ panel's legend() ----------------------------
void legend()
{
    cubeList[0][0].type    = 1;  //  line
    cubeList[0][0].part[0] = 1;  //  1-5
    cubeList[0][0].part[1] = 0;
    
    cubeList[0][1].type    = 2;  //  curve
    cubeList[0][1].part[0] = 1;  //  1-4
    cubeList[0][1].part[1] = 0;
    
    cubeList[0][2].type    = 3;  //  turnout
    cubeList[0][2].part[0] = 8;  //  curve 1-4
    cubeList[0][2].part[1] = 1;  //  line  1-5
    
    cubeList[0][3].type    = 3;  //  turnout
    cubeList[0][3].part[0] = 5;  //  curve 1-6
    cubeList[0][3].part[1] = 1;  //  line  1-5
    
    cubeList[0][4].type    = 4;  //  double-slip
    cubeList[0][4].part[0] = 1;  //  position 1
    cubeList[0][4].part[1] = 0; 

    cubeList[0][5].type    = 5;  //  triple
    cubeList[0][5].part[0] = 1;  //  position 1
    cubeList[0][5].part[1] = 0;

    cubeList[0][6].type    = 6;  //  crossing 90°
    cubeList[0][6].part[0] = 2;  //  line  2-6
    cubeList[0][6].part[1] = 4;  //  line  4-8

    cubeList[0][7].type    = 7;  //  crossing 45°
    cubeList[0][7].part[0] = 1;  //  line  1-5
    cubeList[0][7].part[1] = 2;  //  line  2-6

    cubeList[0][8].type    = 8;  //  buffer-stop
    cubeList[0][8].part[0] = 1;  //  line  1-5
    cubeList[0][8].part[1] = 0;  
}